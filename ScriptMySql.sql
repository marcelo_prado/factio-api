USE DB_FACTIO;

START TRANSACTION;

CREATE TABLE Perfil (
  id INT NOT NULL AUTO_INCREMENT ,
  descricao VARCHAR(50) NOT NULL,
  PRIMARY KEY (id ASC));

CREATE UNIQUE INDEX descricao_UNIQUE ON Perfil (descricao ASC);

CREATE TABLE Usuario (
  id INTEGER  NOT NULL AUTO_INCREMENT,
  nome VARCHAR(40) NOT NULL,
  telefone VARCHAR(13) NOT NULL,
  email VARCHAR(40) UNIQUE NOT NULL,
  senha VARCHAR(20) NOT NULL,
  ativo TINYINT UNSIGNED NOT NULL DEFAULT 1,
  ultimo_login DATETIME(3) NULL,
  fk_id_perf INTEGER NOT NULL,
  PRIMARY KEY(id),
  CONSTRAINT fk_Usuario_Perfil
    FOREIGN KEY (fk_id_perf)
    REFERENCES Perfil (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE Moradia (
  id INTEGER  NOT NULL AUTO_INCREMENT,
  nome VARCHAR(40) NOT NULL,
  endereco VARCHAR(80) NOT NULL,
  numero INTEGER  NOT NULL,
  complemento VARCHAR(40) NULL,
  bairro VARCHAR(40) NOT NULL,
  cidade VARCHAR(40) NOT NULL,
  estado CHAR(2) NOT NULL,
  cep VARCHAR(10) NOT NULL,
  comentario VARCHAR(200) NULL,
  data_criacao DATETIME(3) NOT NULL,
  responsavel INTEGER  NOT NULL,
  PRIMARY KEY(id),
  CONSTRAINT responsavel_moradia_fk FOREIGN KEY(responsavel)
    REFERENCES Usuario(id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Conta (
  id INTEGER  NOT NULL AUTO_INCREMENT,
  tipo VARCHAR(15) NOT NULL,
  valor_total NUMERIC(7,2) NULL,
  data_vencimento DATETIME(3) NOT NULL,
  pago TINYINT UNSIGNED NULL DEFAULT 0,
  moradia_id INTEGER  NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(Moradia_id)
    REFERENCES Moradia(id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Membros_Moradia (
  Usuário_id INTEGER  NOT NULL,
  Moradia_id INTEGER  NOT NULL,
  data_ingresso DATETIME(3) NOT NULL,
  permissao_id INTEGER  NOT NULL,
  PRIMARY KEY(Usuário_id, Moradia_id),
  INDEX usuario_moradia_fk(Usuário_id),
  INDEX moradia_usuario_fk(Moradia_id),
  FOREIGN KEY(Usuário_id)
    REFERENCES Usuario(id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(Moradia_id)
    REFERENCES Moradia(id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Tarefa (
  id INTEGER  NOT NULL AUTO_INCREMENT,
  nome VARCHAR(60) UNIQUE NOT NULL,
  descricao VARCHAR(200) NOT NULL,
  tipo VARCHAR(10) NOT NULL,
  data_criacao DATETIME(3) NOT NULL,
  moradia_id INTEGER  NOT NULL,
  PRIMARY KEY(id),
  INDEX moradia_tarefa_fk(moradia_id),
  FOREIGN KEY(Moradia_id)
    REFERENCES Moradia(id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Alerta (
  id INTEGER  NOT NULL AUTO_INCREMENT,
  moradia_id INTEGER  NOT NULL,
  nome VARCHAR(40) NOT NULL,
  descricao VARCHAR(200) NOT NULL,
  responsavel INTEGER  NOT NULL,
  data_criacao DATETIME(3) NOT NULL,
  data_notificacao DATETIME(3) NULL,
  tarefa_id INTEGER  NOT NULL,
  notificacao TINYINT UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY(id),
  INDEX membros_moradia_alerta_fk(responsavel, moradia_id),
  FOREIGN KEY(responsavel, moradia_id)
    REFERENCES Membros_Moradia(Usuário_id, Moradia_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Anuncio_vaga (
  id INTEGER  NOT NULL AUTO_INCREMENT,
  Membros_Moradia_Moradia_id INTEGER  NOT NULL,
  Membros_Moradia_Usuário_id INTEGER  NOT NULL,
  qtd_quartos INTEGER  NOT NULL,
  descricao LONGTEXT NOT NULL,
  publico_alvo VARCHAR(50) NOT NULL,
  observacoes LONGTEXT NULL,
  PRIMARY KEY(id),
  INDEX anuncio_vaga_membro_moradia_fk(Membros_Moradia_Usuário_id, Membros_Moradia_Moradia_id),
  FOREIGN KEY(Membros_Moradia_Usuário_id, Membros_Moradia_Moradia_id)
    REFERENCES Membros_Moradia(Usuário_id, Moradia_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Responsavel_Tarefa (
  moradia_id INTEGER  NOT NULL,
  usuario_id INTEGER  NOT NULL,
  tarefa_id INTEGER  NOT NULL,
  finalizada TINYINT UNSIGNED NULL DEFAULT 0,
  data_conclusao DATETIME(3) NULL,
  data_atribuicao DATETIME(3) NOT NULL,
  PRIMARY KEY(moradia_id, usuario_id, tarefa_id),
  INDEX membros_responsavel_tarefa_fk(usuario_id, moradia_id),
  INDEX tarefa_responsavel_tarefa_fk(tarefa_id),
  INDEX Responsavel_Tarefa_FKIndex2(tarefa_id),
  FOREIGN KEY(usuario_id, moradia_id)
    REFERENCES Membros_Moradia(Usuário_id, Moradia_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(tarefa_id)
    REFERENCES Tarefa(id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Anuncio_evento (
  id INTEGER  NOT NULL AUTO_INCREMENT,
  data_criacao DATETIME(3) NOT NULL,
  descricao VARCHAR(100) NOT NULL,
  telefone_contato VARCHAR(13) NOT NULL,
  atracoes VARCHAR(100) NOT NULL,
  data_realizacao DATETIME(3) NOT NULL,
  valor_entrada NUMERIC(7,2) NOT NULL,
  usuario_id INTEGER  NOT NULL,
  moradia_id INTEGER  NOT NULL,
  PRIMARY KEY(id),
  CONSTRAINT unico_evento_moradia UNIQUE (moradia_id, data_realizacao),
  INDEX membro_anuncio_evento_fk(usuario_id, moradia_id),
  INDEX moradia_anuncio_evento_fk(moradia_id),
  INDEX Anuncio_evento_FKIndex2(moradia_id),
  FOREIGN KEY(usuario_id, moradia_id)
    REFERENCES Membros_Moradia(Usuário_id, Moradia_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(moradia_id)
    REFERENCES Moradia(id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Conta_Compartilhada (
  conta_id INTEGER  NOT NULL,
  moradia_id INTEGER  NOT NULL,
  usuario_id INTEGER  NOT NULL,
  valor_parcela NUMERIC(7,2) NULL,
  pago TINYINT UNSIGNED NULL DEFAULT 0,
  confirmada TINYINT UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY(conta_id, moradia_id, usuario_id),
  INDEX membros_moradia_conta_fk(usuario_id, moradia_id),
  INDEX conta_membros_moradia_fk(conta_id),
  FOREIGN KEY(usuario_id, moradia_id)
    REFERENCES Membros_Moradia(Usuário_id, Moradia_id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(conta_id)
    REFERENCES Conta(id)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Role (
  id INT NOT NULL AUTO_INCREMENT,
  descricao_role VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE Perfil_has_Role (
  id_perf_role INT NOT NULL,
  fk_id_perf INT NOT NULL,
  fk_id_role INT NOT NULL,
  PRIMARY KEY (id_perf_role),
  CONSTRAINT fk_Perfil_has_Role_Perfil1
    FOREIGN KEY (fk_id_perf)
    REFERENCES Perfil (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Perfil_has_Role_Role1
    FOREIGN KEY (fk_id_role)
    REFERENCES Role (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
CREATE INDEX idx_id_role_perfil_has_role ON Perfil_has_Role (fk_id_role ASC);
CREATE INDEX idx_id_perf_perfil_has_role ON Perfil_has_Role (fk_id_perf ASC);

CREATE TABLE Resetar_senha_token (
  id_resetar_token INT AUTO_INCREMENT  NOT NULL,
  token VARCHAR (60) UNIQUE NOT NULL,
  data_expiracao DATETIME(3) NOT NULL,
  fk_id_usua INT NOT NULL,
  PRIMARY KEY (id_resetar_token),
  CONSTRAINT fk_Resetar_senha_token_Usuario1
    FOREIGN KEY (fk_id_usua)
    REFERENCES Usuario (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
    
CREATE INDEX idx_id_usua_resetar_senha_token ON Resetar_senha_token (fk_id_usua ASC);

INSERT INTO Role(descricao_role) VALUES ('ROLE_USUARIO'), ('ROLE_ADMINISTRADOR'), ('ROLE_USUARIOPREMIUM')
INSERT INTO Perfil(descricao) VALUES ('Usuário'), ('Usuário Premium'), ('Administrador');
INSERT INTO Perfil_has_Role(id_perf_role, fk_id_perf, fk_id_role) VALUES (1, 1, 1), (2, 2, 3), (3, 3, 2), (4, 2, 1), (5, 3, 1), (6, 3, 3);

INSERT INTO Usuario(nome, telefone, email, senha, ativo, ultimo_login, fk_id_perf) VALUES ('Marcelo Prado', '34999999478', 'marcelinhprado@gmail.com', '12345', 1, null, 3);
INSERT INTO Usuario(nome, telefone, email, senha, ativo, ultimo_login, fk_id_perf) VALUES ('Rhaniel Christian', '34999998888', 'rhaniel@gmail.com', '12345', 1, null, 3);

COMMIT;

