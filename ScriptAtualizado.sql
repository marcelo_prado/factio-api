CREATE DATABASE  IF NOT EXISTS `db_factio` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_factio`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_factio
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.30-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alerta`
--

DROP TABLE IF EXISTS `alerta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alerta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(40) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `responsavel` int(11) NOT NULL,
  `data_criacao` datetime(3) NOT NULL,
  `data_notificacao` datetime(3) DEFAULT NULL,
  `tarefa_id` int(11) NOT NULL,
  `notificacao` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `anuncio_evento`
--

DROP TABLE IF EXISTS `anuncio_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anuncio_evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime(3) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `telefone_contato` varchar(13) NOT NULL,
  `atracoes` varchar(100) NOT NULL,
  `data_realizacao` datetime(3) NOT NULL,
  `valor_entrada` decimal(7,2) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `moradia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unico_evento_moradia` (`moradia_id`,`data_realizacao`),
  KEY `moradia_anuncio_evento_fk` (`moradia_id`),
  KEY `Anuncio_evento_FKIndex2` (`moradia_id`),
  CONSTRAINT `anuncio_evento_ibfk_2` FOREIGN KEY (`moradia_id`) REFERENCES `moradia` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `anuncio_vaga`
--

DROP TABLE IF EXISTS `anuncio_vaga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anuncio_vaga` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moradia_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `qtd_quartos` int(11) NOT NULL,
  `descricao` longtext NOT NULL,
  `publico_alvo` varchar(50) NOT NULL,
  `observacoes` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conta`
--

DROP TABLE IF EXISTS `conta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(15) NOT NULL,
  `valor_total` decimal(7,2) DEFAULT NULL,
  `data_vencimento` datetime(3) NOT NULL,
  `pago` tinyint(3) unsigned DEFAULT '0',
  `moradia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `moradia_id` (`moradia_id`),
  CONSTRAINT `conta_ibfk_1` FOREIGN KEY (`moradia_id`) REFERENCES `moradia` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conta_compartilhada`
--

DROP TABLE IF EXISTS `conta_compartilhada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conta_compartilhada` (
  `conta_id` int(11) NOT NULL,
  `moradia_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `valor_parcela` decimal(7,2) DEFAULT NULL,
  `pago` tinyint(3) unsigned DEFAULT '0',
  `confirmada` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`conta_id`,`moradia_id`,`usuario_id`),
  KEY `conta_membros_moradia_fk` (`conta_id`),
  CONSTRAINT `conta_compartilhada_ibfk_2` FOREIGN KEY (`conta_id`) REFERENCES `conta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moradia`
--

DROP TABLE IF EXISTS `moradia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moradia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(40) NOT NULL,
  `endereco` varchar(80) NOT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(40) DEFAULT NULL,
  `bairro` varchar(40) NOT NULL,
  `cidade` varchar(40) NOT NULL,
  `estado` char(2) NOT NULL,
  `cep` varchar(10) NOT NULL,
  `comentario` varchar(200) DEFAULT NULL,
  `data_criacao` datetime(3) NOT NULL,
  `responsavel` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `responsavel_moradia_fk` (`responsavel`),
  CONSTRAINT `responsavel_moradia_fk` FOREIGN KEY (`responsavel`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `descricao_UNIQUE` (`descricao`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `perfil_has_role`
--

DROP TABLE IF EXISTS `perfil_has_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil_has_role` (
  `id_perf_role` int(11) NOT NULL,
  `fk_id_perf` int(11) NOT NULL,
  `fk_id_role` int(11) NOT NULL,
  PRIMARY KEY (`id_perf_role`),
  KEY `idx_id_role_perfil_has_role` (`fk_id_role`),
  KEY `idx_id_perf_perfil_has_role` (`fk_id_perf`),
  CONSTRAINT `fk_Perfil_has_Role_Perfil1` FOREIGN KEY (`fk_id_perf`) REFERENCES `perfil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Perfil_has_Role_Role1` FOREIGN KEY (`fk_id_role`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resetar_senha_token`
--

DROP TABLE IF EXISTS `resetar_senha_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resetar_senha_token` (
  `id_resetar_token` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(60) NOT NULL,
  `data_expiracao` datetime(3) NOT NULL,
  `fk_id_usua` int(11) NOT NULL,
  PRIMARY KEY (`id_resetar_token`),
  UNIQUE KEY `token` (`token`),
  KEY `idx_id_usua_resetar_senha_token` (`fk_id_usua`),
  CONSTRAINT `fk_Resetar_senha_token_Usuario1` FOREIGN KEY (`fk_id_usua`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `responsavel_tarefa`
--

DROP TABLE IF EXISTS `responsavel_tarefa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responsavel_tarefa` (
  `usuario_id` int(11) NOT NULL,
  `tarefa_id` int(11) NOT NULL,
  `finalizada` tinyint(3) unsigned DEFAULT '0',
  `data_conclusao` datetime(3) DEFAULT NULL,
  `data_atribuicao` datetime(3) NOT NULL,
  PRIMARY KEY (`usuario_id`,`tarefa_id`),
  KEY `tarefa_responsavel_tarefa_fk` (`tarefa_id`),
  KEY `Responsavel_Tarefa_FKIndex2` (`tarefa_id`),
  CONSTRAINT `responsavel_tarefa_ibfk_2` FOREIGN KEY (`tarefa_id`) REFERENCES `tarefa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao_role` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tarefa`
--

DROP TABLE IF EXISTS `tarefa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarefa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `tipo` varchar(10) NOT NULL,
  `data_criacao` datetime(3) NOT NULL,
  `moradia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`),
  KEY `moradia_tarefa_fk` (`moradia_id`),
  CONSTRAINT `tarefa_ibfk_1` FOREIGN KEY (`moradia_id`) REFERENCES `moradia` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(40) NOT NULL,
  `telefone` varchar(13) NOT NULL,
  `email` varchar(40) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `ativo` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `ultimo_login` datetime DEFAULT NULL,
  `data_ingresso` datetime DEFAULT NULL,
  `fk_id_perf` int(11) NOT NULL,
  `fk_id_moradia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `fk_Usuario_Perfil` (`fk_id_perf`),
  KEY `fk_Usuario_Moradia_idx` (`fk_id_moradia`),
  CONSTRAINT `fk_Usuario_Moradia` FOREIGN KEY (`fk_id_moradia`) REFERENCES `moradia` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_Perfil` FOREIGN KEY (`fk_id_perf`) REFERENCES `perfil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'db_factio'
--

--
-- Dumping routines for database 'db_factio'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-13  0:03:50
