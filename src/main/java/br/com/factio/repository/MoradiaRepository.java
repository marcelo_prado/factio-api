package br.com.factio.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.factio.repository.entity.Moradia;

@Repository
public interface MoradiaRepository extends JpaRepository<Moradia, Long>, QueryByExampleExecutor<Moradia> {
	Optional<Moradia> findById(Long id);
	
	Optional<Moradia> findByApelido(String apelido);
	
	Optional<Moradia> findByResponsavelId(Long usuario);
	
	@Query("select m from Moradia m inner join Usuario u on u.moradia.id = m.id where u.id = ?1")
	Optional<Moradia> findByMembroId(Long membroId);
}
