package br.com.factio.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.factio.repository.entity.AnuncioVaga;

@Repository
public interface AnuncioVagaRepository extends JpaRepository<AnuncioVaga, Long>{
	@Query("select a from AnuncioVaga a left join Moradia m on a.moradia.id = m.id where m.estado = ?1")
	Page<AnuncioVaga> findByEstado(String estado, Pageable pageable);
	
	@Query("select a from AnuncioVaga a left join Moradia m on a.moradia.id = m.id "
			+ " where m.estado = ?1 and m.cidade like concat('%', ?2, '%')")
	Page<AnuncioVaga> findByEstadoCidade(String estado, String cidade, Pageable pageable);
}
