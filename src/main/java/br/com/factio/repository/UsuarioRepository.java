package br.com.factio.repository;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.factio.repository.entity.Usuario;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>, QueryByExampleExecutor<Usuario> {

    Optional<Usuario> findByEmail(String usuarioEmail);
    
    Optional<Usuario> findById(Long id);
    
    Optional<Usuario> findByUltimoLogin(Date ultimoLogin);

}
