package br.com.factio.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.factio.repository.entity.Perfil;

/**
 *  
 * @author Grupo 2 - PDSI 
 */
@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long> {
	Optional<Perfil> findById(Long id);
}
