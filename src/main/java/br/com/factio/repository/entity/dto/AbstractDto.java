package br.com.factio.repository.entity.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.factio.repository.entity.IEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public abstract class AbstractDto<T extends IEntity> implements Serializable {

    private static final long serialVersionUID = -2073739232968101656L; 
    
    @JsonIgnore
    protected final T entity;
    
}
