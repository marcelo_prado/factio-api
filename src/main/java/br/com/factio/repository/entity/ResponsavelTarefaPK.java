package br.com.factio.repository.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class ResponsavelTarefaPK implements Serializable {
	private static final long serialVersionUID = -3163322690300241881L;

	@Column(name = "usuario_id")
	private Long usuarioId;

	@Column(name = "tarefa_id")
	private Long tarefaId;
}
