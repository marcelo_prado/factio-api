package br.com.factio.repository.entity.dto;

import java.util.Date;

import br.com.factio.repository.entity.Usuario;
import lombok.Getter;

@Getter
public class UsuarioDto extends AbstractDto<Usuario> {

    private static final long serialVersionUID = 5201473045637388938L;

    private long id;
    private String nome;
    private String telefone;
    private String email;
    private Boolean  ativo;
    private Date ultimoLogin;
    private PerfilDto perfil;
    private MoradiaMinimalDto moradia;
    private String latitude;
    private String longitude;
    private Boolean ehResponsavel;
    
    public UsuarioDto(Usuario entity) {
        super(entity);
        this.id = entity.getId();
        this.nome = entity.getNome();
        this.telefone = entity.getTelefone();
        this.email = entity.getEmail();
        this.ativo = entity.getAtivo();
        this.ultimoLogin = entity.getUltimoLogin();
        this.perfil = new PerfilDto(entity.getPerfil());
        if(entity.getMoradia() != null) {
        	this.moradia = new MoradiaMinimalDto(entity.getMoradia());
        }
        this.latitude = entity.getLatitude();
        this.longitude = entity.getLongitude();
        this.ehResponsavel = entity.getMoradiaResponsavel() != null ? true : false;
    }
}
