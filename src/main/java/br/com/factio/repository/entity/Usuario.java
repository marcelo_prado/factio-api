package br.com.factio.repository.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Grupo 2 - PDSI
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false, of = "id")
@Entity
@Table(name = "Usuario", uniqueConstraints = { @UniqueConstraint(columnNames = { "email" }) })
public class Usuario implements Serializable, IEntity {

	private static final long serialVersionUID = 7191649161284716589L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@NotNull
	@Size(max = 100)
	@Column(name = "nome")
	private String nome;

	@NotNull
	@Size(max = 13)
	@Column(name = "telefone")
	private String telefone;

	@NotNull
	@Size(max = 100)
	@Column(name = "email")
	private String email;

	@NotNull
	@Size(max = 20)
	@Column(name = "senha")
	private String senha;

	@NotNull
	@Column(name = "ativo")
	private Boolean ativo;

	@NotNull
	@ManyToOne(targetEntity = Perfil.class)
	@JoinColumn(name = "fk_id_perf", nullable = false)
	private Perfil perfil;

	@Column(name = "ultimo_login")
	private Date ultimoLogin;

	@Column(name = "data_ingresso")
	private Date dataIngresso;

	@OneToOne(mappedBy = "responsavel")
	@Cascade(CascadeType.ALL)
	private Moradia moradiaResponsavel;

	@ManyToOne(targetEntity = Moradia.class, fetch = FetchType.LAZY)
	@Cascade(CascadeType.SAVE_UPDATE)
	@JoinColumn(name = "fk_id_moradia")
	private Moradia moradia;
	
	@Size(max = 30)
	@Column(name = "latitude")
	private String latitude;
	
	@Size(max = 30)
	@Column(name = "longitude")
	private String longitude;
	
	@Column(name = "horaLocalizacao")
	private Date horaLocalizacao;

	@OneToMany(mappedBy = "usuario", orphanRemoval = true)
	@Cascade(CascadeType.MERGE)
	private List<ResponsavelTarefa> tarefas = new ArrayList<>();

	public void addTarefa(Tarefa tarefa, Date dataConclusao) {		
		ResponsavelTarefa responsavelTarefa = new ResponsavelTarefa(this, tarefa, dataConclusao);
		tarefas.add(responsavelTarefa);
		tarefa.getUsuarios().add(responsavelTarefa);
	}

	public void removeTarefa(Tarefa tarefa) {
		for (Iterator<ResponsavelTarefa> iterator = tarefas.iterator(); iterator.hasNext();) {
			ResponsavelTarefa responsavelTarefa = iterator.next();

			if (responsavelTarefa.getUsuario().equals(this) && responsavelTarefa.getTarefa().equals(tarefa)) {
				iterator.remove();
				responsavelTarefa.getTarefa().getUsuarios().remove(responsavelTarefa);
				responsavelTarefa.setUsuario(null);
				responsavelTarefa.setTarefa(null);
			}
		}
	}
}
