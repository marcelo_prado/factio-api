package br.com.factio.repository.entity.dto;

import java.util.Date;

import br.com.factio.repository.entity.Moradia;
import lombok.Getter;

@Getter
public class MoradiaDto extends AbstractDto<Moradia>{
	
	private static final long serialVersionUID = 4287475372324416062L;
	
	private Long id;	
	private String nome;	
	private String endereco;	
	private int numero;	
	private String complemento;	
	private String bairro;	
	private String cidade;	
	private String estado;	
	private String cep;	
	private Date dataCriacao;
	
	public MoradiaDto(Moradia entity) {
		super(entity);	
		this.id = entity.getId();
		this.nome = entity.getNome();
		this.endereco = entity.getEndereco();
		this.numero = entity.getNumero();
		this.complemento = entity.getComplemento();
		this.bairro = entity.getBairro();
		this.cidade = entity.getCidade();
		this.estado = entity.getEstado();				
		this.cep = entity.getCep();
		this.dataCriacao = entity.getDataCriacao();
	}	
}
