package br.com.factio.repository.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Grupo 2 - PDSI
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false, of = "id")
@Entity
@Table(name = "Tarefa")
public class Tarefa implements Serializable, IEntity{	
	private static final long serialVersionUID = 1552269153031340497L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
	
    @NotNull
    @Size(max = 60)
    @Column(name = "nome")
    private String nome;
    
    @NotNull
    @Size(max = 200)
    @Column(name = "descricao")
    private String descricao;
    
    @NotNull
    @Size(max = 50)
    @Column(name = "tipo")
    private String tipo;
    
    @NotNull
    @Column(name = "data_criacao")
    private Date dataCriacao;
    
    @NotNull
	@ManyToOne(targetEntity = Moradia.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "moradia_id", nullable = false)	
	private Moradia moradia;
    
    @OneToMany(mappedBy = "tarefa")
    @Cascade({CascadeType.MERGE, CascadeType.DELETE})
    private List<ResponsavelTarefa> usuarios = new ArrayList<>();
}
