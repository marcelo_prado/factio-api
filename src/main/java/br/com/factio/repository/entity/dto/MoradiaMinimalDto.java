package br.com.factio.repository.entity.dto;

import br.com.factio.repository.entity.Moradia;
import lombok.Getter;

@Getter
public class MoradiaMinimalDto extends AbstractDto<Moradia>{
		
	private static final long serialVersionUID = -6854871214452093744L;
	
	private Long id;	
	private String nome;	
	
	public MoradiaMinimalDto(Moradia entity) {
		super(entity);	
		this.id = entity.getId();
		this.nome = entity.getNome();
	}	
}
