package br.com.factio.repository.entity.dto;

import br.com.factio.repository.entity.Usuario;
import lombok.Getter;

@Getter
public class UsuarioMinimalDto extends AbstractDto<Usuario>{
	private static final long serialVersionUID = -7228958638936933562L;
	
	private Long id;	
	private String nome;	
	
	public UsuarioMinimalDto(Usuario entity) {
		super(entity);	
		this.id = entity.getId();
		this.nome = entity.getNome();
	}	
}
