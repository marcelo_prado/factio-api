package br.com.factio.repository.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false, of = "id")
@Entity
@Table(name = "anuncio_evento")
public class AnuncioEvento implements Serializable, IEntity{
	
	private static final long serialVersionUID = 6936643587928915443L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@NotNull
	@Column(name = "data_criacao")
	private Date dataCriacao;
	
	@Size(max = 45)
	@NotNull
	@Column(name = "nome")
	private String nome;
	
	@Size(max = 100)
	@NotNull
	@Column(name = "descricao")
	private String descricao;
	
	@Size(max = 13)	
	@NotNull
	@Column(name = "telefone_contato")
	private String telefoneContato;
	
	@Size(max = 1000)
	@NotNull
	@Column(name = "atracoes")
	private String atracoes;
	
	@NotNull
	@Column(name = "data_realizacao")
	private Date dataRealizacao;
	
	@NotNull
	@Column(name = "valor_entrada")
	private BigDecimal valorEntrada;
	
	@NotNull
	@ManyToOne(targetEntity = Moradia.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "moradia_id", nullable = false)
	@Cascade(CascadeType.ALL)
	private Moradia moradia;
	
	@NotNull
	@ManyToOne(targetEntity = Usuario.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id")
	@Cascade(CascadeType.ALL)
	private Usuario criador;
}
