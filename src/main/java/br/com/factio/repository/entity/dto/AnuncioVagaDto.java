package br.com.factio.repository.entity.dto;

import br.com.factio.repository.entity.AnuncioVaga;
import lombok.Getter;

@Getter
public class AnuncioVagaDto extends AbstractDto<AnuncioVaga>{	
	private static final long serialVersionUID = 3181360296521110390L;
	
	private Long id;
	private Integer qtdQuartos;
	private String descricao;
	private String publicoAlvo;
	private String observacoes;
	private MoradiaDto moradia;
	private Long criador;
	private String nomeCriador;
	
	public AnuncioVagaDto(AnuncioVaga entity) {
		super(entity);
		this.id = entity.getId();
		this.qtdQuartos = entity.getQtdQuartos();
		this.descricao = entity.getDescricao();
		this.publicoAlvo = entity.getPublicoAlvo();
		this.observacoes = entity.getObservacoes();
		this.moradia = new MoradiaDto(entity.getMoradia());
		this.criador = entity.getCriador().getId();
		this.nomeCriador = entity.getCriador().getNome();
	}
}
