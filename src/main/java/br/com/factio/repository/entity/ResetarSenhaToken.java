package br.com.factio.repository.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false, of = "id")
@Entity
@Table(name = "Resetar_senha_token")
public class ResetarSenhaToken implements IEntity {
    public static int getTempoExpiracao() {
        return TEMPO_EXPIRACAO;
    }

    private static final int TEMPO_EXPIRACAO = 15 * 60;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_resetar_token")
    private int id;
    
    @Column(name = "token")
    private String token;
    
    @Column(name = "data_expiracao")
    private Date dataExpiracao;
    
    @OneToOne(targetEntity = Usuario.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_id_usua")
    private Usuario usuario;
}
