package br.com.factio.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false, of = "id")
@Entity
@Table(name = "Role")
public class Role implements IEntity {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	@Column(name = "id")
	private long id;
	
	@Column(name = "descricao_role")
	private String descricao;	
}
