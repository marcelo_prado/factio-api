package br.com.factio.repository.entity.dto;

import br.com.factio.repository.entity.Perfil;
import lombok.Getter;

public class PerfilDto extends AbstractDto<Perfil> {

    private static final long serialVersionUID = 2990727621847104438L;

    @Getter
    private long id;
    @Getter
    private String descricao;

    public PerfilDto(Perfil entity) {
        super(entity);
        this.id = entity.getId();
        this.descricao = entity.getDescricao();
    }

}
