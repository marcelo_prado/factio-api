package br.com.factio.repository.entity.dto;

import java.math.BigDecimal;
import java.util.Date;

import br.com.factio.repository.entity.AnuncioEvento;
import lombok.Getter;

@Getter
public class AnuncioEventoDto extends AbstractDto<AnuncioEvento> {
	private static final long serialVersionUID = 2353384537901471910L;
	
	private Long id;	
	private String nome;
	private Date dataCriacao;
	private String descricao;
	private String telefoneContato;
	private String atracoes;
	private Date dataRealizacao;
	private BigDecimal valorEntrada;
	private MoradiaDto moradia;
	private Long criador;
	
	public AnuncioEventoDto(AnuncioEvento entity) {
		super(entity);
		
		this.id = entity.getId();
		this.nome = entity.getNome();
		this.dataCriacao = entity.getDataCriacao();
		this.descricao = entity.getDescricao();
		this.telefoneContato = entity.getTelefoneContato();
		this.atracoes = entity.getAtracoes();
		this.dataRealizacao = entity.getDataRealizacao();
		this.valorEntrada = entity.getValorEntrada();
		this.moradia = new MoradiaDto(entity.getMoradia());
		this.criador = entity.getCriador().getId();
	}
}
