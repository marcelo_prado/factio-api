package br.com.factio.repository.entity.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.factio.repository.entity.ResponsavelTarefa;
import br.com.factio.repository.entity.Tarefa;
import lombok.Getter;

@Getter
public class TarefaDto extends AbstractDto<Tarefa>{
	private static final long serialVersionUID = 4504483377848511509L;
	
	private Long id;
    private String nome;
    private String descricao;
    private String tipo;
    private Date dataCriacao;
    private Date dataConclusao;
    private Long moradia;
    private List<Long> responsaveis;
    private Boolean completa;
    
    public TarefaDto(Tarefa entity) {
    	super(entity);
    	this.id = entity.getId();
    	this.nome = entity.getNome();
    	this.descricao = entity.getDescricao();
    	this.tipo = entity.getTipo();
    	this.dataCriacao = entity.getDataCriacao();
    	this.moradia = entity.getMoradia().getId();
    }
    
    public TarefaDto(Tarefa entity, List<ResponsavelTarefa> responsaveis) {
    	super(entity);
    	this.id = entity.getId();
    	this.nome = entity.getNome();
    	this.descricao = entity.getDescricao();
    	this.tipo = entity.getTipo();
    	this.dataCriacao = entity.getDataCriacao();
    	this.moradia = entity.getMoradia().getId();    	
    	this.responsaveis = new ArrayList<>();
    	Date dataConclusao = null;
    	boolean completa = false;
    	for(ResponsavelTarefa rt : responsaveis) {
    		this.responsaveis.add(rt.getUsuario().getId());
    		dataConclusao = rt.getDataConclusao();
    		if(rt.getFinalizada() != null && rt.getFinalizada())
    			completa = true;
    	}
    	this.dataConclusao = dataConclusao;
    	this.completa = completa;
    }
}
