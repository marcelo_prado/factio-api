package br.com.factio.repository.entity.dto;

import java.util.Date;

import br.com.factio.repository.entity.SolicitacaoMoradia;
import lombok.Getter;

@Getter
public class SolicitacaoMoradiaDto extends AbstractDto<SolicitacaoMoradia>{
	private static final long serialVersionUID = 2319355567840215086L;
	
	private Long usuarioId;
	private String nomeUsuario;
	private Long moradiaId;
	private String nomeMoradia;
	private Date dataSolicitacao;
	
	public SolicitacaoMoradiaDto(SolicitacaoMoradia entity) {
		super(entity);
		this.usuarioId = entity.getUsuario().getId();
		this.nomeUsuario = entity.getUsuario().getNome();
		this.moradiaId = entity.getMoradia().getId();
		this.nomeMoradia = entity.getMoradia().getNome();
		this.dataSolicitacao = entity.getDataSolicitacao();
	}
}
