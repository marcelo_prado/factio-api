package br.com.factio.repository.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false, of = "id")
@Entity
@Table(name = "Moradia")
public class Moradia implements Serializable, IEntity{
	
	private static final long serialVersionUID = -4154729723567138272L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@NotNull
    @Size(max = 30)
    @Column(name = "apelido")
    private String apelido;
	
	@Size(max = 40)
	@Column(name = "nome")
	@NotNull
	private String nome;
	
	@Size(max = 80)
	@Column(name = "endereco")
	@NotNull
	private String endereco;
	
	@Column(name = "numero")
	@NotNull
	private Integer numero;
	
	@Size(max = 40)
	@Column(name = "complemento")
	private String complemento;
	
	@Size(max = 40)
	@Column(name = "bairro")
	@NotNull
	private String bairro;
	
	@Size(max = 40)
	@Column(name = "cidade")
	@NotNull
	private String cidade;
	
	@Size(max = 2)
	@Column(name = "estado")
	@NotNull
	private String estado;
	
	@Size(max = 10)
	@Column(name = "cep")
	@NotNull
	private String cep;
	
	@Size(max = 200)
	@Column(name = "comentario")
	private String comentario;
	
	@Column(name = "data_criacao")
	@NotNull
	private Date dataCriacao;
		
	@OneToOne
	@JoinColumn(name="responsavel")
	@NotNull
	private Usuario responsavel;
	
	@OneToMany(mappedBy="moradia")
	private List<Usuario> membros;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="moradia")
	private List<AnuncioEvento> anuncioEventos;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="moradia")
	private List<AnuncioVaga> anuncioVagas;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="moradia")
	private List<SolicitacaoMoradia> solicitacoesIngresso;
}
