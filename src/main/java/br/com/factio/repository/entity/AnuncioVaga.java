package br.com.factio.repository.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false, of = "id")
@Entity
@Table(name = "anuncio_vaga")
public class AnuncioVaga implements Serializable, IEntity{
	private static final long serialVersionUID = 777061819129061506L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@NotNull
	@Column(name = "qtd_quartos")
	private Integer qtdQuartos;
	
	@NotNull
	@Column(name = "descricao")
	private String descricao;
	
	@Size(max = 50)	
	@NotNull
	@Column(name = "publico_alvo")
	private String publicoAlvo;
	
	@Size(max = 1000)
	@NotNull
	@Column(name = "observacoes")
	private String observacoes;
	
	@NotNull
	@ManyToOne(targetEntity = Moradia.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "moradia_id", nullable = false)
	@Cascade(CascadeType.ALL)
	private Moradia moradia;
	
	@NotNull
	@ManyToOne(targetEntity = Usuario.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = false)
	@Cascade(CascadeType.ALL)
	private Usuario criador;
}
