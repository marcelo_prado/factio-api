package br.com.factio.repository.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "responsavel_tarefa")
public class ResponsavelTarefa implements Serializable, IEntity{
	private static final long serialVersionUID = -6181815669851804952L;
	
	public ResponsavelTarefa(Usuario usuario, Tarefa tarefa, Date dataConclusao) {
		this.usuario = usuario;
		this.tarefa = tarefa;
		this.dataConclusao = dataConclusao;
		this.id = new ResponsavelTarefaPK(usuario.getId(), tarefa.getId());
	}

	@EmbeddedId
	private ResponsavelTarefaPK id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("usuarioId")
	private Usuario usuario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("tarefaId")
	private Tarefa tarefa;
	
	@Column(name = "finalizada")
	private Boolean finalizada;
	
	@Column(name = "qualidade_tarefa")
	private Integer qualidadeTarefa;
	
	@NotNull
	@Column(name = "data_atribuicao")
	private Date dataAtribuicao = Calendar.getInstance().getTime();
	
	@Column(name = "data_conclusao")
	private Date dataConclusao;
}
