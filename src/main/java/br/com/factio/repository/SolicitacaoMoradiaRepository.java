package br.com.factio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.factio.repository.entity.SolicitacaoMoradia;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@Repository
public interface SolicitacaoMoradiaRepository extends JpaRepository<SolicitacaoMoradia, Long> {
	List<SolicitacaoMoradia> findByMoradiaId(Long id);
	
	// @Modifying
	Long deleteByMoradiaIdAndUsuarioId(Long moradiaId, Long usuarioId);
}
