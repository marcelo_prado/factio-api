package br.com.factio.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.factio.repository.entity.Perfil;
import br.com.factio.repository.entity.PerfilRole;
import br.com.factio.repository.entity.Role;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */
public interface PerfilRoleRepository extends JpaRepository<PerfilRole, Serializable>{	
	
	List<PerfilRole> findByPerfil(Perfil perfil);

    /**
     * Busca os ids dos roles vinculados ao perfil por id
     * 
     * @param perfil
     * @return Lista de ids dos roles vinculados ao perfil
     */
	//TODO
    //@Query("select pr.fk_role_id from perfil_has_role pr where pr.fk_id_perf = ?1")
    List<Long> findIdRolesByPerfilId(Long perfil);
    
    /**
     * Busca roles vinculados ao perfils por id
     * 
     * @param perfil
     * @return Lista de roles vinculados ao perfil
     */
    //TODO
    //@Query("select pr.fk_id_role from perfil_has_role pr where pr.fk_id_perf = ?1")
    List<Role> findRolesByPerfil(Perfil perfil);
    
    /**
     * Remove os vinculos perfil x role para o perfil informado.
     * 
     * @param perfil
     * @return long Quantidade de registros removidos.
     */
    Long deleteByPerfil(Perfil perfil);
}
