package br.com.factio.repository;

import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.factio.repository.entity.ResetarSenhaToken;
import br.com.factio.repository.entity.Usuario;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */
@Repository
public interface ResetarSenhaTokenRepository extends JpaRepository<ResetarSenhaToken, Long> {
    Optional<ResetarSenhaToken> findByToken(String token);

    Optional<ResetarSenhaToken> findByUsuario(Usuario usuario);

    Stream<ResetarSenhaToken> findAllByDataExpiracaoLessThan(Date now);
    
    void deleteByToken(String token);
    
    void deleteByDataExpiracaoLessThan(Date now);    

    @Modifying
    @Query("delete from ResetarSenhaToken where dataExpiracao <= ?1")
    void deleteAllExpiredSince(Date now);
}
