package br.com.factio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.factio.repository.entity.Role;

/**
 *  
 * @author Grupo 2 - PDSI 
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

}
