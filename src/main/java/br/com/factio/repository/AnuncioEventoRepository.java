package br.com.factio.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.factio.repository.entity.AnuncioEvento;

@Repository
public interface AnuncioEventoRepository extends JpaRepository<AnuncioEvento, Long>, QueryByExampleExecutor<AnuncioEvento>  {
	List<AnuncioEvento> findByNomeContains(String nome);
	
	@Query("select a from AnuncioEvento a left join Moradia m on a.moradia.id = m.id where m.estado = ?1")
	Page<AnuncioEvento> findByEstado(String estado, Pageable pageable);
	
	@Query("select a from AnuncioEvento a left join Moradia m on a.moradia.id = m.id "
			+ " where m.estado = ?1 and m.cidade like concat('%', ?2, '%')")
	Page<AnuncioEvento> findByEstadoCidade(String estado, String cidade, Pageable pageable);
}
