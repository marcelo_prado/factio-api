package br.com.factio;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import br.com.factio.config.ApplicationConfig;
import br.com.factio.config.ApplicationConfigDatabase;

/**
 * Propriedades de configuracao do contexto da base de dados.
 * 
 * @author Grupo 2 - PDSI
 * @since 1.0
 */
@Configuration
public class ApplicationDatabaseContext {

    @Autowired
    private ApplicationConfig appConfig;

    @Bean
    public DataSource datasource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setUsername(this.appConfig.getAppConfigDatabase().getDatabaseUsername());
        dataSource.setPassword(this.appConfig.getAppConfigDatabase().getDatabasePassword());
        dataSource.setUrl(this.appConfig.getAppConfigDatabase().getDatabaseUrl());
        dataSource.setDriverClassName(this.appConfig.getAppConfigDatabase().getDriverClassName());

        return dataSource;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setEntityManagerFactory(entityManagerFactory);
        return manager;
    }

    @Bean
    @Autowired
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setShowSql(this.appConfig.getAppConfigDatabase().getShowSql());
        adapter.setDatabasePlatform(appConfig.getAppConfigDatabase().getDatabasePlatform());

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setDataSource(dataSource);
        factory.setPackagesToScan(this.appConfig.getBasePackages());
        factory.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        factory.setJpaVendorAdapter(adapter);
        factory.setJpaProperties(configureJpaProperties());
        return factory;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    /**
     * Configura as propriedades jpa do datasource.
     * 
     * @return Properties
     */
    private Properties configureJpaProperties() {
        final ApplicationConfigDatabase dbConfig = this.appConfig.getAppConfigDatabase();
        final Properties jpaProperties = new Properties();
        jpaProperties.put(org.hibernate.cfg.Environment.DEFAULT_SCHEMA, dbConfig.getDatabaseName());
        jpaProperties.put(org.hibernate.cfg.Environment.SHOW_SQL, dbConfig.getShowSql());
        return jpaProperties;
    }

}

