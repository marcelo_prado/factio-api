package br.com.factio.config;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Properties configurations used in the application.
 */
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Component
public class ApplicationConfig implements Serializable {

    private static final long serialVersionUID = 7896624401620243915L;

    @Getter
    @Value("${basePackages}")
    private String basePackages;

    @Getter
    @Value("${applicationName}")
    private String applicationName;

    @Getter
    @Value("${environmentName}")
    private String environmentName;

    @Getter
    @Value("${support.email}")
    private String supportEmail;
    
    @Getter
    @Value("${factio.web.url}")
    private String urlfactioWeb;
    
    @Getter
    @Value("${factio.web.url.recuperarSenha}")
    private String urlRecuperarSenha;
    
    @Getter
    @Autowired
    private ApplicationConfigDatabase appConfigDatabase;

}
