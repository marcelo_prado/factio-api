package br.com.factio.config;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Propriedades de configuracao da base de dados.
 * 
 * @author Grupo 2 - PDSI
 * @since 1.0
 */
@NoArgsConstructor
@Component
public class ApplicationConfigDatabase implements Serializable {

    private static final long serialVersionUID = 7896624401620243915L;

    @Getter
    @Value("${database.databaseName}")
    private String databaseName;

    @Getter
    @Value("${database.databasePlatform}")
    private String databasePlatform;

    @Getter
    @Value("${database.url}")
    private String databaseUrl;

    @Getter
    @Value("${database.username}")
    private String databaseUsername;

    @Getter
    @Value("${database.password}")
    private String databasePassword;

    @Getter
    @Value("${database.show.sql}")
    private Boolean showSql;

    @Getter
    @Value("${database.driverClassName}")
    private String driverClassName;
}
