package br.com.factio.enums;

import lombok.Getter;

/**
 * Enumeration containing the keys for internalization. 
 */
public enum Mensagens {
    
	ServicoExecutadoSucesso("msg.servico.execucao.sucesso", "Serviço executado com sucesso"),
	FalhaExecutarServico("msg.servico.execucao.falha", "O serviço não foi executado com sucesso"),
	
	NotFound("msg.exception.notfound", "Não foi possível localizar o recurso"),
	BadRequest("msg.exception.badrequest", "Os dados informados na requisição estão incorretos"),
	BadCredentials("msg.exception.badcredentials", "Email e/ou senha não correspondentes"),
	InternalServerError("msg.exception.internalerror", "Houve um erro no processamento da requisição dentro do servidor"),
	Unauthorized("msg.exception.unauthorized", "Acesso não autorizado"),
	
	EmailJaCadastrado("msg.email.ja.cadastrado", "E-mail já cadastrado no sistema"),	 
	SenhaAlterada("msg.senha.alterada", "Senha alterada com sucesso"), 	
	UsuarioNaoEncontrado("msg.usuario.nao.encontrado", "O usuário não foi encontrado"),
	UsuarioRegistrado("msg.usuario.registrado", "O usuário foi registrado com sucesso"),
	UsuarioAtualizado("msg.usuario.atualizado", "O usuário foi atualizado com sucesso"),
	UsuarioVinculadoMoradia("msg.usuario.vinculado.moradia", "O usuário informado já está vinculado em uma moradia. Para continuar, desvincule-o e tente novamente!"),
	PerfilNaoEncontrado("msg.perfil.nao.encontrado", "O perfil não foi encontrado"),
	
	TokenRegistradoSucesso("msg.token.registrado.com.sucesso", "O token para recuperação de senha foi enviado com sucesso"),        
	TokenExpirado("msg.token.expirado", "O token informado está expirado"),
	TokenValido("msg.token.valido", "O token informado é válido"),
	TokenInvalido("msg.token.invalido", "O token informado é inválido"),
	
	MoradiaNaoEncontrada("msg.moradia.nao.encontrada","A moradia informada não foi encontrada"),
	MoradiaRegistrada("msg.moradia.registrada", "A moradia foi registrada com sucesso"),
	MoradiaAtualizada("msg.moradia.atualizada", "A moradia foi atualizada com sucesso"),
	UsuarioResponsavel("msg.usuario.responsavel", "Apenas o usuário responsável pela moradia pode atualizar estas informações"),
	UsuarioMoradiaResponsavel("msg.usuario.moradia.responsavel", "O usuário informado já está responsável por outra moradia"),
	ApelidoJaUtilizado("msg.apelido.ja.utilizado", "O apelido informado já está em uso"),
	
	InserirUsuarioMoradia("msg.inserir.usuario.moradia", "O usuário foi inserido na moradia com sucesso"),
	
	AnuncioEventoNaoEncontrado("msg.anuncioevento.nao.encontrado", "O anúncio de evento informado não foi encontrado"),
	AnuncioEventoRegistrado("msg.anuncioevento.registrado", "O anúncio de evento foi registrado com sucesso"),
	AnuncioEventoAtualizado("msg.anuncioevento.atualizado", "O anúncio de enveto foi atualizado com sucesso"),
	
	EventoInformeEstado("msg.informe.estado", "Necessário informar o estado nas buscas de eventos por cidade"),
	VagaInformeEstado("msg.informe.estado", "Necessário informar o estado nas buscas de vagas por cidade"),
	
	AnuncioVagaNaoEncontrado("msg.anunciovaga.nao.encontrado", "O anúncio de vaga informado não foi encontrado"),
	AnuncioVagaRegistrado("msg.anunciovaga.registrado", "O anúncio de vaga foi registrado com sucesso"),
	AnuncioVagaAtualizado("msg.anunciovaga.atualizado", "O anúncio de vaga foi atualizado com sucesso"),
	
	TarefaNaoEncontrada("msg.tarefa.nao.encontrada", "A tarefa informada não foi encontrada"),
	TarefaExistente("msg.tarefa.existente", "Já existe uma tarefa com o nome informado"),
	TarefaRegistrada("msg.tarefa.registrada", "A tarefa foi registrada com sucesso"),
	TarefaAtualizada("msg.tarefa.atualizada", "A tarefa foi atualizada com sucesso"),
	TarefaAtribuida("msg.tarefa.atribuida", "A tarefa foi atribuida ao usuário com sucesso"),
	TarefaJaVinculada("msg.tarefa.ja.vinculada", "A tarefa informada já está vinculada a este responsável"),
	DataConclusaoInvalida("msg.dataconclusao.invalida", "A data de conclusão informada para esta tarefa é inválida"),
	TarefaConcluida("msg.tarefa.concluida", "A tarefa foi concluída com sucesso"),
	TarefaDeletada("msg.tarefa.deletada", "A tarefa foi deletada com sucesso"),
	
	SolicitacaoEnviada("msg.solicitacao.enviada", "A solicitação de ingresso na moradia foi enviada com sucesso"), 
	SolicitacaoRemovida("msg.solicitacao.removida", "A solicitação foi removida com sucesso"),
	
	LocalizacaoSalva("msg.localizacao.salva", "A localização foi salva com sucesso");
	
    @Getter
    String key; 
    
    @Getter
    String value;
    
    private Mensagens(String key, String value) {
		this.key = key;
		this.value = value;
    }
}
