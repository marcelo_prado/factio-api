package br.com.factio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.factio.enums.Mensagens;
import lombok.Getter;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends ValidationException {

    private static final long serialVersionUID = 186860191815866877L;

    @Getter
    private String message;
    
    public ResourceNotFoundException() {
        super(Mensagens.NotFound.getKey(), Mensagens.NotFound.getValue());
    }
    
    public ResourceNotFoundException(Mensagens mensagem) {
        super(mensagem.getKey(), mensagem.getValue());
    }
    
    public ResourceNotFoundException(String message) {
        super(Mensagens.NotFound.getKey(), message);
    }
    
    public ResourceNotFoundException(String key, String message) {
        super(key, message);
    }
}
