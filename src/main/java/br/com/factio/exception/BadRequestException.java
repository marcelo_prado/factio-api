package br.com.factio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.factio.enums.Mensagens;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends ValidationException {

    private static final long serialVersionUID = 3958073540896230014L;
    
    public BadRequestException() {
        super(Mensagens.BadRequest.getKey(), Mensagens.BadRequest.getValue());
    }
    
    public BadRequestException(Mensagens mensagem) {
        super(mensagem.getKey(), mensagem.getValue());
    }
    
    public BadRequestException(String message) {
        super(Mensagens.BadRequest.getKey(), message);
    }
    
    public BadRequestException(String key, String message) {
        super(key, message);
    }
}
