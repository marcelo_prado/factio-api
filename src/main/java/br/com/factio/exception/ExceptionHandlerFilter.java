package br.com.factio.exception;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.factio.enums.Mensagens;
import br.com.factio.payload.ApiResponse;

@SuppressWarnings("rawtypes")
public class ExceptionHandlerFilter extends OncePerRequestFilter {
	
	@Override
	public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			filterChain.doFilter(request, response);
		} catch (BadRequestException e) {
			ApiResponse errorResponse = new ApiResponse(Mensagens.TokenInvalido);
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			response.getWriter().write(convertObjectToJson(errorResponse));
			response.setContentType("application/json; charset=UTF-8");
		} catch (Exception e) {
			ApiResponse errorResponse = new ApiResponse(Mensagens.InternalServerError);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.getWriter().write(convertObjectToJson(errorResponse));
			response.setContentType("application/json; charset=UTF-8");
		}
	}

	private String convertObjectToJson(Object object) throws JsonProcessingException {
		if (object == null) {
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}

}
