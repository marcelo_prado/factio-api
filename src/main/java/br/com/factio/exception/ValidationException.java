package br.com.factio.exception;

import org.springframework.http.HttpStatus;

import br.com.factio.enums.Mensagens;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ValidationException extends RuntimeException {

    private static final long serialVersionUID = 3473094129626736298L;
    
    /**
     * Status code representa o valor que será devolvido a requisição HTTP, por default este valor é
     * 400 (BAD_REQUEST).
     */
    @Getter
    private int statusCode = HttpStatus.BAD_REQUEST.value();

    @Getter
    private String key, value;
    
    public ValidationException(Mensagens mensagem) {
        super();
        this.key = mensagem.getKey();
        this.value = mensagem.getValue();
    }
    
    public ValidationException(String key, String value) {
        super();
        this.key = key;
        this.value = value;
    }    
    
    public ValidationException(Throwable t, String key, String value) {
        super(t);
        this.key = key;
        this.value = value;
    }

    public ValidationException(Throwable t, int statusCode, String key, String value) {
        super(t);
        this.statusCode = statusCode;
        this.key = key;
        this.value = value;
    }
}
