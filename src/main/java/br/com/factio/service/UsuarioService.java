package br.com.factio.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import br.com.factio.payload.AlterarSenhaRequest;
import br.com.factio.payload.AlterarSenhaTokenRequest;
import br.com.factio.payload.AtualizarUsuarioRequest;
import br.com.factio.payload.PagedResponse;
import br.com.factio.payload.SalvarLocalizacaoRequest;
import br.com.factio.payload.SalvarUsuarioRequest;
import br.com.factio.repository.entity.Role;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.repository.entity.dto.UsuarioDto;
import br.com.factio.security.UsuarioPrincipal;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@Service
public interface UsuarioService {
    void alterarSenha(UsuarioPrincipal usuarioLogado, AlterarSenhaRequest alterarSenhaRequest);

    void alterarSenhaToken(AlterarSenhaTokenRequest alterarSenhaTokenRequest);

    void atualizarUsuario(UsuarioPrincipal usuarioLogado, AtualizarUsuarioRequest atualizarUsuarioRequest);

    void salvarUsuario(SalvarUsuarioRequest salvarUsuarioRequest);
    
    void salvarLocalizacao(UsuarioPrincipal usuarioLogado, SalvarLocalizacaoRequest salvarUsuarioRequest);

    boolean existsByEmail(String email);

    List<Role> consultarPermissoesPorIdPerfil(Long id);

    Usuario findById(Long id);

    Usuario findByEmail(String email);

    Usuario findByToken(String token);

    PagedResponse<UsuarioDto> findAllByExample(Map<String, String> filters, int page, int size);    
}
