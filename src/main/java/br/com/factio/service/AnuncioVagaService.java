package br.com.factio.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import br.com.factio.payload.PagedResponse;
import br.com.factio.payload.SalvarAnuncioVagaRequest;
import br.com.factio.repository.entity.dto.AnuncioVagaDto;
import br.com.factio.security.UsuarioPrincipal;

@Service
public interface AnuncioVagaService {
	void salvarAnuncioVaga(UsuarioPrincipal usuarioLogado, SalvarAnuncioVagaRequest salvarAnuncioVagaRequest);	

	AnuncioVagaDto findById(Long id);
	
	PagedResponse<AnuncioVagaDto> findAll(Map<String, String> filters, int page, int size);
}
