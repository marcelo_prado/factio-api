package br.com.factio.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.factio.enums.Mensagens;
import br.com.factio.exception.BadRequestException;
import br.com.factio.exception.ResourceNotFoundException;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.AlterarSenhaRequest;
import br.com.factio.payload.AlterarSenhaTokenRequest;
import br.com.factio.payload.AtualizarUsuarioRequest;
import br.com.factio.payload.PagedResponse;
import br.com.factio.payload.SalvarLocalizacaoRequest;
import br.com.factio.payload.SalvarUsuarioRequest;
import br.com.factio.repository.PerfilRepository;
import br.com.factio.repository.PerfilRoleRepository;
import br.com.factio.repository.UsuarioRepository;
import br.com.factio.repository.entity.Perfil;
import br.com.factio.repository.entity.PerfilRole;
import br.com.factio.repository.entity.ResetarSenhaToken;
import br.com.factio.repository.entity.Role;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.repository.entity.dto.UsuarioDto;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.ResetarSenhaTokenService;
import br.com.factio.service.UsuarioService;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private PerfilRoleRepository perfilRoleRepository;

    @Autowired
    private PerfilRepository perfilRepository;

    @Autowired
    private ResetarSenhaTokenService resetarSenhaTokenService;

    @Override
    public boolean existsByEmail(String email) {
        return usuarioRepository.findByEmail(email).isPresent();
    }

    @Transactional
    @Override
    public void atualizarUsuario(UsuarioPrincipal usuarioLogado, AtualizarUsuarioRequest atualizarUsuarioRequest) {
    	if(usuarioLogado.getUsuario().getId() == atualizarUsuarioRequest.getId()) {
	        Usuario usuario = findById(atualizarUsuarioRequest.getId());
	
	        usuario.setNome(atualizarUsuarioRequest.getNome());
	        usuario.setTelefone(atualizarUsuarioRequest.getTelefone());
	        usuario.setAtivo(atualizarUsuarioRequest.isAtivo());
	        Perfil perfil = perfilRepository.findById(atualizarUsuarioRequest.getPerfil())
	            .orElseThrow(() -> new ResourceNotFoundException(Mensagens.PerfilNaoEncontrado));
	        usuario.setPerfil(perfil);
	
	        if (atualizarUsuarioRequest.getUltimoLogin() != null) {
	            usuario.setUltimoLogin(atualizarUsuarioRequest.getUltimoLogin());
	        }
	
	        usuarioRepository.save(usuario);
        }
    	else
    		throw new BadRequestException(Mensagens.Unauthorized);
    }

    @Transactional
    @Override
    public void salvarUsuario(SalvarUsuarioRequest salvarUsuarioRequest) {
        if (this.existsByEmail(salvarUsuarioRequest.getEmail()))
            throw new ValidationException(Mensagens.EmailJaCadastrado);

        Usuario usuario = new Usuario();
        usuario.setNome(salvarUsuarioRequest.getNome());
        usuario.setTelefone(salvarUsuarioRequest.getTelefone());
        usuario.setEmail(salvarUsuarioRequest.getEmail());        
        usuario.setAtivo(salvarUsuarioRequest.isAtivo());
        usuario.setSenha(salvarUsuarioRequest.getSenha());
        // usuario.setSenha(passwordEncoder.encode(salvarUsuarioRequest.getSenha()));

        Perfil perfil = perfilRepository.findById(salvarUsuarioRequest.getPerfil())
            .orElseThrow(() -> new ResourceNotFoundException(Mensagens.PerfilNaoEncontrado));
        usuario.setPerfil(perfil);

        // Salva o usuário
        usuarioRepository.save(usuario);
    }
    
    @Transactional
    @Override
    public void salvarLocalizacao(UsuarioPrincipal usuarioLogado, SalvarLocalizacaoRequest salvarLocalizacaoRequest) {
    	if(usuarioLogado.getUsuario().getId() == salvarLocalizacaoRequest.getUsuarioId()) {
	        Usuario usuario = findById(salvarLocalizacaoRequest.getUsuarioId());
	        usuario.setLongitude(salvarLocalizacaoRequest.getLongitude());
	        usuario.setLatitude(salvarLocalizacaoRequest.getLatitude()); 
	        usuario.setHoraLocalizacao(salvarLocalizacaoRequest.getHoraLocalizacao());
	        usuarioRepository.save(usuario);
        }
    	else
    		throw new BadRequestException(Mensagens.Unauthorized);
    }

    @Transactional
    @Override
    public void alterarSenha(UsuarioPrincipal usuarioLogado, AlterarSenhaRequest alterarSenhaRequest) {
        Usuario usuario = usuarioLogado.getUsuario();
        if (usuario.getSenha().equals(alterarSenhaRequest.getSenhaAntiga())) {
            usuario.setSenha(alterarSenhaRequest.getSenhaNova());
            usuarioRepository.save(usuario);
        } else {
            throw new BadRequestException();
        }
    }

    @Transactional
    @Override
    public void alterarSenhaToken(AlterarSenhaTokenRequest alterarSenhaRequest) {
        String token = alterarSenhaRequest.getToken();
        String novaSenha = alterarSenhaRequest.getSenhaNova();
        resetarSenhaTokenService.validarToken(token);
        Usuario usuario = this.findByToken(token);

        // Alterando a senha do usuário após validar o token
        usuario.setSenha(novaSenha);
        usuarioRepository.save(usuario);

        // Removendo o token validado
        resetarSenhaTokenService.deletarToken(token);
    }

    @Override
    public List<Role> consultarPermissoesPorIdPerfil(Long perfilId) {
        List<Role> roles = new ArrayList<>();
        if (perfilId != null) {
            Perfil p = new Perfil();
            p.setId(perfilId);

            List<PerfilRole> perfilRoles = perfilRoleRepository.findByPerfil(p);

            for (PerfilRole perfilRole : perfilRoles) {
                roles.add(perfilRole.getRole());
            }
        }
        return roles;
    }

    @Override
    public Usuario findById(Long id) {
        Usuario usuario = usuarioRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException(Mensagens.UsuarioNaoEncontrado));

        return usuario;
    }

    @Override
    public Usuario findByEmail(String email) {
        Usuario usuario = usuarioRepository.findByEmail(email)
            .orElseThrow(() -> new ResourceNotFoundException(Mensagens.UsuarioNaoEncontrado));

        return usuario;
    }

    @Override
    public Usuario findByToken(String token) {
        ResetarSenhaToken resetarSenhaToken = resetarSenhaTokenService.findByToken(token);

        Usuario usuario = resetarSenhaToken.getUsuario();

        return usuario;
    }

    @Override
    public PagedResponse<UsuarioDto> findAllByExample(Map<String, String> filters, int page, int size) {
        Usuario usuario = new Usuario();
        usuario.setNome(filters.get("nome"));
        usuario.setEmail(filters.get("email"));
        usuario.setTelefone(filters.get("telefone"));
        
        Perfil perfil;
        if (filters.get("perfil") != null) {
            perfil = perfilRepository.findById(Long.parseLong(filters.get("perfil"))).get();
            usuario.setPerfil(perfil);
        }        

        if (filters.get("ativo") != null)
            usuario.setAtivo(Boolean.parseBoolean(filters.get("ativo")));

        ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("nome", match -> match.contains())
            .withIgnoreNullValues().withIgnoreCase();

        Example<Usuario> exemploUsuario = Example.of(usuario, matcher);
        
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "nome");
        Page<Usuario> usuarios = usuarioRepository.findAll(exemploUsuario, pageable);
		
        List<UsuarioDto> usuariosDto = new ArrayList<UsuarioDto>();
        for (Usuario u : usuarios.getContent()) {
            usuariosDto.add(new UsuarioDto(u));
        }

        PagedResponse<UsuarioDto> pagedResponse = new PagedResponse<>(usuariosDto, usuarios.getNumber(), usuarios.getSize(),
            usuarios.getTotalElements(), usuarios.getTotalPages(), usuarios.isLast());
        
        return pagedResponse;
    }
}
