package br.com.factio.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.factio.enums.Mensagens;
import br.com.factio.exception.BadRequestException;
import br.com.factio.exception.ResourceNotFoundException;
import br.com.factio.payload.AtualizarMoradiaRequest;
import br.com.factio.payload.InserirMembroRequest;
import br.com.factio.payload.SalvarMoradiaRequest;
import br.com.factio.repository.MoradiaRepository;
import br.com.factio.repository.SolicitacaoMoradiaRepository;
import br.com.factio.repository.UsuarioRepository;
import br.com.factio.repository.entity.Moradia;
import br.com.factio.repository.entity.SolicitacaoMoradia;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.repository.entity.dto.MoradiaDto;
import br.com.factio.repository.entity.dto.SolicitacaoMoradiaDto;
import br.com.factio.repository.entity.dto.UsuarioDto;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.MoradiaService;
import br.com.factio.service.UsuarioService;

@Service
public class MoradiaServiceImpl implements MoradiaService {
	@Autowired
	private MoradiaRepository moradiaRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private SolicitacaoMoradiaRepository solicitacaoMoradiaRepository;
	
	@Autowired
	private UsuarioService usuarioService;

	@Transactional
	@Override
	public void salvarMoradia(UsuarioPrincipal usuarioLogado, SalvarMoradiaRequest salvarMoradiaRequest) {
		// O usuário que requisitou a inserção da moradia deve possuir o id do
		// responsável no corpo da requisição
		if (usuarioLogado.getUsuario().getId() != salvarMoradiaRequest.getResponsavel()) {
			throw new BadRequestException(Mensagens.Unauthorized);
		}

		// O usuário não pode estar vinculado a nenhuma outra moradia
		if (usuarioLogado.getUsuario().getMoradiaResponsavel() != null
				|| usuarioLogado.getUsuario().getMoradia() != null) {
			throw new BadRequestException(Mensagens.UsuarioVinculadoMoradia);
		}

		// O apelido da moradia deve ser único
		if (moradiaRepository.findByApelido(salvarMoradiaRequest.getApelido()).isPresent()) {
			throw new BadRequestException(Mensagens.ApelidoJaUtilizado);
		}

		// Cria a moradia com os parametros recebidos
		Moradia moradia = new Moradia();
		moradia.setApelido(salvarMoradiaRequest.getApelido());
		moradia.setNome(salvarMoradiaRequest.getNome());
		moradia.setEndereco(salvarMoradiaRequest.getEndereco());
		moradia.setNumero(salvarMoradiaRequest.getNumero());
		moradia.setComplemento(salvarMoradiaRequest.getComplemento());
		moradia.setComentario(salvarMoradiaRequest.getComentario());
		moradia.setBairro(salvarMoradiaRequest.getBairro());
		moradia.setCidade(salvarMoradiaRequest.getCidade());
		moradia.setEstado(salvarMoradiaRequest.getEstado());
		moradia.setCep(salvarMoradiaRequest.getCep());
		moradia.setDataCriacao(new Date());

		Usuario responsavel = usuarioService.findById(salvarMoradiaRequest.getResponsavel());
		moradia.setResponsavel(responsavel);
		responsavel.setMoradia(moradia);

		// Persiste o usuário e moradia no banco de dados
		moradiaRepository.save(moradia);
		usuarioRepository.save(responsavel);
	}

	@Transactional
	@Override
	public void atualizarMoradia(UsuarioPrincipal usuarioLogado, AtualizarMoradiaRequest atualizarMoradiaRequest) {
		Moradia moradia = findById(atualizarMoradiaRequest.getId());

		// O usuário que requisitou a atualização da moradia deve possuir o id do
		// responsável no corpo da
		// requisição
		if (usuarioLogado.getUsuario().getId() == moradia.getResponsavel().getId()) {
			// Atualiza os campos da moradia com os parâmetros recebidos
			moradia.setNome(atualizarMoradiaRequest.getNome());
			moradia.setEndereco(atualizarMoradiaRequest.getEndereco());
			moradia.setNumero(atualizarMoradiaRequest.getNumero());
			moradia.setComplemento(atualizarMoradiaRequest.getComplemento());
			moradia.setComentario(atualizarMoradiaRequest.getComentario());
			moradia.setBairro(atualizarMoradiaRequest.getBairro());
			moradia.setCidade(atualizarMoradiaRequest.getCidade());
			moradia.setEstado(atualizarMoradiaRequest.getEstado());
			moradia.setCep(atualizarMoradiaRequest.getCep());

			Long responsavel = atualizarMoradiaRequest.getResponsavel();
			// Atualizar o responsável pela moradia
			if (responsavel != null && responsavel != usuarioLogado.getUsuario().getId()) {
				Usuario usuario = usuarioRepository.findById(responsavel)
						.orElseThrow(() -> new ResourceNotFoundException(Mensagens.UsuarioNaoEncontrado));

				if (usuario.getMoradiaResponsavel() == null) {
					moradia.setResponsavel(usuario);
				} else {
					throw new BadRequestException(Mensagens.UsuarioMoradiaResponsavel);
				}
			}

			// Persiste a moradia no banco de dados
			moradiaRepository.save(moradia);
		} else
			throw new BadRequestException(Mensagens.UsuarioResponsavel);
	}

	@Override
	public Moradia findById(Long id) {
		Moradia moradia = moradiaRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(Mensagens.MoradiaNaoEncontrada));

		return moradia;
	}

	@Override
	public List<Moradia> findAll() {
		List<Moradia> moradias = moradiaRepository.findAll();

		return moradias;
	}

	@Override
	public Page<Moradia> findAllByExample(Map<String, String> filters, Pageable pageable) {
		Moradia moradia = new Moradia();

		// Realiza uma busca personalizada de uma moradia com base em vários parâmetros
		// da entidade
		moradia.setNome(filters.get("nome"));
		moradia.setEndereco(filters.get("endereco"));
		moradia.setBairro(filters.get("bairro"));
		moradia.setCidade(filters.get("cidade"));
		moradia.setEstado(filters.get("estado"));
		moradia.setCep(filters.get("cep"));

		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

		Date dataFormatada;
		if (filters.get("dataCriacao") != null) {
			try {
				dataFormatada = formato.parse(filters.get("dataCriacao"));
				moradia.setDataCriacao(dataFormatada);
			} catch (ParseException e) {
			}
		}

		Usuario responsavel;
		if (filters.get("responsavel") != null) {
			responsavel = usuarioRepository.findById(Long.parseLong(filters.get("responsavel"))).get();
			moradia.setResponsavel(responsavel);
		}

		ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("nome", match -> match.contains())
				.withMatcher("endereco", match -> match.contains()).withIgnoreNullValues().withIgnoreCase();

		Example<Moradia> exemploMoradia = Example.of(moradia, matcher);

		return moradiaRepository.findAll(exemploMoradia, pageable);
	}

	@Transactional
	@Override
	public void inserirMembro(InserirMembroRequest membro, UsuarioPrincipal usuarioLogado) {
		// Apenas o usuário responsável pela moradia pode requisitar a inserção de um
		// novo membro
		Usuario usuario = usuarioLogado.getUsuario();
		Long idMoradia = usuario.getMoradiaResponsavel() != null ? usuario.getMoradiaResponsavel().getId() : null;

		if (idMoradia != null && idMoradia.compareTo(membro.getMoradia()) == 0) {
			Usuario novoUsuario = usuarioService.findById(membro.getUsuario());

			if (novoUsuario.getMoradia() != null || novoUsuario.getMoradiaResponsavel() != null)
				throw new BadRequestException(Mensagens.UsuarioVinculadoMoradia);

			Moradia moradia = this.findById(membro.getMoradia());
			List<Usuario> membros = moradia.getMembros();

			novoUsuario.setDataIngresso(Calendar.getInstance().getTime());
			novoUsuario.setMoradia(moradia);
			usuarioRepository.save(novoUsuario);

			membros.add(novoUsuario);
			moradia.setMembros(membros);
			moradiaRepository.save(moradia);
			
			// Remoção da solicitação de ingresso neste caso (se houver)
			this.removerSolicitacao(membro, usuarioLogado);
		} else {
			throw new BadRequestException(Mensagens.Unauthorized);
		}
	}
	
	@Transactional
	@Override
	public Boolean removerSolicitacao(InserirMembroRequest solicitacao, UsuarioPrincipal usuarioLogado) {
		// Apenas o usuário responsável pela moradia pode requisitar a remoção de uma solicitação
		// de ingresso
		Usuario usuario = usuarioLogado.getUsuario();
		Long idMoradia = usuario.getMoradiaResponsavel() != null ? usuario.getMoradiaResponsavel().getId() : null;

		if (idMoradia != null && idMoradia.compareTo(solicitacao.getMoradia()) == 0) {
			return solicitacaoMoradiaRepository.deleteByMoradiaIdAndUsuarioId(solicitacao.getMoradia(), solicitacao.getUsuario()) > 0L;
			
		} else {
			throw new BadRequestException(Mensagens.Unauthorized);
		}
	}
	
	@Transactional
	@Override
	public void solicitarIngresso(InserirMembroRequest solicitarIngresso, UsuarioPrincipal usuarioLogado) {
		if (usuarioLogado.getUsuario().getId() == solicitarIngresso.getUsuario()) {
			SolicitacaoMoradia solicitacao = new SolicitacaoMoradia();
			
			Usuario usuario = usuarioService.findById(solicitarIngresso.getUsuario());
			Moradia moradia = moradiaRepository.findById(solicitarIngresso.getMoradia())
					.orElseThrow(() -> new ResourceNotFoundException(Mensagens.MoradiaNaoEncontrada));
			
			solicitacao.setMoradia(moradia);
			solicitacao.setUsuario(usuario);
			solicitacao.setDataSolicitacao(new Date());
			solicitacaoMoradiaRepository.save(solicitacao);		
		} else {
			throw new BadRequestException(Mensagens.Unauthorized);
		}
	}

	@Override
	public List<UsuarioDto> listarMembros(Long moradiaId, UsuarioPrincipal usuarioLogado) {
		Usuario usuario = usuarioLogado.getUsuario();
		Long idMoradia = usuario.getMoradia() != null ? usuario.getMoradia().getId() : null;

		// Cada usuário pode visualizar apenas membros de sua moradia
		if (idMoradia != null && idMoradia.compareTo(moradiaId) == 0) {
			Moradia moradia = moradiaRepository.findById(moradiaId)
					.orElseThrow(() -> new ResourceNotFoundException(Mensagens.MoradiaNaoEncontrada));
			List<UsuarioDto> usuariosDto = new ArrayList<>();

			for (Usuario u : moradia.getMembros()) {
				usuariosDto.add(new UsuarioDto(u));
			}

			return usuariosDto;
		} else {
			throw new BadRequestException(Mensagens.Unauthorized);
		}
	}
	
	@Override
	public List<SolicitacaoMoradiaDto> listarSolicitacoes(Long moradiaId, UsuarioPrincipal usuarioLogado) {
		Usuario usuario = usuarioLogado.getUsuario();
		Long idMoradia = usuario.getMoradia() != null ? usuario.getMoradia().getId() : null;

		// Cada usuário pode visualizar apenas solicitações de ingresso na sua moradia
		if (idMoradia != null && idMoradia.compareTo(moradiaId) == 0) {
			List<SolicitacaoMoradia> solicitacoes = solicitacaoMoradiaRepository.findByMoradiaId(moradiaId);
			List<SolicitacaoMoradiaDto> solicitacoesDto = new ArrayList<>();
			
			for(SolicitacaoMoradia s : solicitacoes)
				solicitacoesDto.add(new SolicitacaoMoradiaDto(s));
			
			return solicitacoesDto;
		} else {
			throw new BadRequestException(Mensagens.Unauthorized);
		}		
	}

	@Override
	public MoradiaDto listarMoradiaPorMembro(Long idMembro, UsuarioPrincipal usuarioLogado) {		
		if(idMembro.compareTo(usuarioLogado.getUsuario().getId()) != 0)
			throw new BadRequestException(Mensagens.Unauthorized);
		
		Moradia moradia = moradiaRepository.findByMembroId(idMembro)
				.orElseThrow(() -> new ResourceNotFoundException(Mensagens.MoradiaNaoEncontrada));
		
		return new MoradiaDto(moradia);
	}
}
