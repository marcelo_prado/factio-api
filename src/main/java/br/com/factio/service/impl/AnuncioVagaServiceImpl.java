package br.com.factio.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.factio.enums.Mensagens;
import br.com.factio.exception.BadRequestException;
import br.com.factio.exception.ResourceNotFoundException;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.PagedResponse;
import br.com.factio.payload.SalvarAnuncioVagaRequest;
import br.com.factio.repository.AnuncioVagaRepository;
import br.com.factio.repository.MoradiaRepository;
import br.com.factio.repository.entity.AnuncioVaga;
import br.com.factio.repository.entity.Moradia;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.repository.entity.dto.AnuncioVagaDto;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.AnuncioVagaService;
import br.com.factio.service.UsuarioService;

@Service
public class AnuncioVagaServiceImpl implements AnuncioVagaService{

	@Autowired
	private AnuncioVagaRepository anuncioVagaRepository;
	
	@Autowired
	private MoradiaRepository moradiaRepository;
	
	@Autowired
	private UsuarioService usuarioService;

	@Override
	public void salvarAnuncioVaga(UsuarioPrincipal usuarioLogado,
			SalvarAnuncioVagaRequest salvarAnuncioVagaRequest) {
		if (usuarioLogado.getUsuario().getId().compareTo(salvarAnuncioVagaRequest.getCriadorId()) != 0
				|| (usuarioLogado.getUsuario().getMoradia().getId().compareTo(salvarAnuncioVagaRequest.getMoradiaId()) != 0))
			throw new ValidationException(Mensagens.Unauthorized);
		
		AnuncioVaga anuncioVaga = new AnuncioVaga();
		anuncioVaga.setId(salvarAnuncioVagaRequest.getId());
		anuncioVaga.setDescricao(salvarAnuncioVagaRequest.getDescricao());
		anuncioVaga.setQtdQuartos(salvarAnuncioVagaRequest.getQtdQuartos());
		anuncioVaga.setPublicoAlvo(salvarAnuncioVagaRequest.getPublicoAlvo());
		anuncioVaga.setObservacoes(salvarAnuncioVagaRequest.getObservacoes());
		
		Usuario criador = usuarioService.findById(salvarAnuncioVagaRequest.getCriadorId());
		
		Moradia moradia = moradiaRepository.findById(salvarAnuncioVagaRequest.getMoradiaId())
				.orElseThrow(() -> new ResourceNotFoundException(Mensagens.MoradiaNaoEncontrada));
		
		anuncioVaga.setMoradia(moradia);
		anuncioVaga.setCriador(criador);
		
		anuncioVagaRepository.save(anuncioVaga);
	}

	@Override
	public AnuncioVagaDto findById(Long id) {
		AnuncioVaga anuncioVaga = anuncioVagaRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(Mensagens.AnuncioVagaNaoEncontrado));
		return new AnuncioVagaDto(anuncioVaga);
	}

	@Override
	public PagedResponse<AnuncioVagaDto> findAll(Map<String, String> filters, int page, int size) {
		String cidade = filters.get("cidade");
		String estado = filters.get("estado");
				
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "qtdQuartos");
		Page<AnuncioVaga> anuncioVagas;
		
		// Busca por cidade e estado
		if (cidade != null && estado != null) {
			anuncioVagas = anuncioVagaRepository.findByEstadoCidade(estado, cidade, pageable);			
		} else if (estado != null && cidade == null) {			
			anuncioVagas = anuncioVagaRepository.findByEstado(estado, pageable);			
		} else if(estado == null && cidade == null){
			anuncioVagas = anuncioVagaRepository.findAll(pageable);
		} else {
			throw new BadRequestException(Mensagens.VagaInformeEstado);
		}
		
		List<AnuncioVagaDto> anuncioVagasDto = new ArrayList<>();
        for (AnuncioVaga anuncio : anuncioVagas.getContent()) {
            anuncioVagasDto.add(new AnuncioVagaDto(anuncio));
        }

        PagedResponse<AnuncioVagaDto> pagedResponse = new PagedResponse<>(anuncioVagasDto, anuncioVagas.getNumber(), anuncioVagas.getSize(),
        		anuncioVagas.getTotalElements(), anuncioVagas.getTotalPages(), anuncioVagas.isLast());
        
        return pagedResponse;
	}

}
