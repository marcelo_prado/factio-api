package br.com.factio.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.factio.config.ApplicationConfig;
import br.com.factio.enums.Mensagens;
import br.com.factio.exception.ResourceNotFoundException;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.RecuperarSenhaRequest;
import br.com.factio.repository.ResetarSenhaTokenRepository;
import br.com.factio.repository.entity.ResetarSenhaToken;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.service.EmailService;
import br.com.factio.service.ResetarSenhaTokenService;
import br.com.factio.service.UsuarioService;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */
@Service
public class ResetarSenhaTokenServiceImpl implements ResetarSenhaTokenService {
    @Autowired
    private ResetarSenhaTokenRepository resetarSenhaTokenRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private ApplicationConfig appConfig;

    @Autowired
    private EmailService emailService;
    
    @Override
	public void registraToken(ResetarSenhaToken resetarSenhaToken) {
		this.resetarSenhaTokenRepository.save(resetarSenhaToken);		
	}

    @Override
    public void resetarSenhaToken(RecuperarSenhaRequest recuperarSenha) {
        // Registra o token
        ResetarSenhaToken resetarSenhaToken = new ResetarSenhaToken();
        Usuario usuario = usuarioService.findByEmail(recuperarSenha.getEmail());
        String token = UUID.randomUUID().toString();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.SECOND, ResetarSenhaToken.getTempoExpiracao());

        resetarSenhaToken.setDataExpiracao(calendar.getTime());
        resetarSenhaToken.setToken(token);
        resetarSenhaToken.setUsuario(usuario);

        this.registraToken(resetarSenhaToken);

        // Envia o email
        // TODO
        SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
        passwordResetEmail.setFrom(appConfig.getSupportEmail());
        passwordResetEmail.setTo(usuario.getEmail());
        passwordResetEmail.setSubject("Password Reset Request");
        passwordResetEmail.setText("Para resetar sua senha, clique no link abaixo:\n" + appConfig.getUrlfactioWeb()
            + appConfig.getUrlRecuperarSenha() + "?token=" + resetarSenhaToken.getToken());

        emailService.sendEmail(passwordResetEmail);
    }

    @Override
    public void validarToken(String token) {
        ResetarSenhaToken resetarSenhaToken = resetarSenhaTokenRepository.findByToken(token)
            .orElseThrow(() -> new ResourceNotFoundException(Mensagens.TokenInvalido));

        Calendar cal = Calendar.getInstance();
        if ((resetarSenhaToken.getDataExpiracao().getTime() - cal.getTime().getTime()) <= 0) {
            throw new ValidationException(Mensagens.TokenExpirado);
        }
    }

    @Transactional
    @Override
    public void deletarTokensExpirados(Date date) {
        resetarSenhaTokenRepository.deleteAllExpiredSince(date);
    }

    @Transactional
    @Override
    public void deletarToken(String token) {
        resetarSenhaTokenRepository.deleteByToken(token);
    }

    @Override
    public ResetarSenhaToken findByUsuario(Usuario usuario) {
        return resetarSenhaTokenRepository.findByUsuario(usuario)
            .orElseThrow(() -> new ResourceNotFoundException(Mensagens.TokenInvalido.getKey()));
    }

    @Override
    public ResetarSenhaToken findByToken(String token) {
        ResetarSenhaToken resetarSenhaToken = resetarSenhaTokenRepository.findByToken(token)
            .orElseThrow(() -> new ResourceNotFoundException(Mensagens.TokenInvalido));

        return resetarSenhaToken;
    }
}
