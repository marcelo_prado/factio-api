package br.com.factio.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.factio.enums.Mensagens;
import br.com.factio.exception.BadRequestException;
import br.com.factio.exception.ResourceNotFoundException;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.PagedResponse;
import br.com.factio.payload.SalvarAnuncioEventoRequest;
import br.com.factio.repository.AnuncioEventoRepository;
import br.com.factio.repository.MoradiaRepository;
import br.com.factio.repository.entity.AnuncioEvento;
import br.com.factio.repository.entity.Moradia;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.repository.entity.dto.AnuncioEventoDto;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.AnuncioEventoService;
import br.com.factio.service.UsuarioService;

@Service
public class AnuncioEventoServiceImpl implements AnuncioEventoService{
	@Autowired
	private AnuncioEventoRepository anuncioEventoRepository;
	
	@Autowired
	private MoradiaRepository moradiaRepository;
	
	@Autowired
	private UsuarioService usuarioService;

	@Override
	public void salvarAnuncioEvento(UsuarioPrincipal usuarioLogado,
			SalvarAnuncioEventoRequest salvarAnuncioEventoRequest) {
		if (usuarioLogado.getUsuario().getId().compareTo(salvarAnuncioEventoRequest.getCriadorId()) != 0
				|| (usuarioLogado.getUsuario().getMoradia().getId().compareTo(salvarAnuncioEventoRequest.getMoradiaId()) != 0))
			throw new ValidationException(Mensagens.Unauthorized);
					
		AnuncioEvento anuncioEvento = new AnuncioEvento();
		anuncioEvento.setId(salvarAnuncioEventoRequest.getId());
		anuncioEvento.setNome(salvarAnuncioEventoRequest.getNome());
		anuncioEvento.setAtracoes(salvarAnuncioEventoRequest.getAtracoes());
		anuncioEvento.setTelefoneContato(salvarAnuncioEventoRequest.getTelefoneContato());
		anuncioEvento.setDataCriacao(Calendar.getInstance().getTime());
		anuncioEvento.setDescricao(salvarAnuncioEventoRequest.getDescricao());
		anuncioEvento.setDataRealizacao(salvarAnuncioEventoRequest.getDataRealizacao());
		anuncioEvento.setValorEntrada(salvarAnuncioEventoRequest.getValorEntrada());
		
		Usuario criador = usuarioService.findById(salvarAnuncioEventoRequest.getCriadorId());		
		
		Moradia moradia = moradiaRepository.findById(salvarAnuncioEventoRequest.getMoradiaId())
				.orElseThrow(() -> new ResourceNotFoundException(Mensagens.MoradiaNaoEncontrada));
		
		anuncioEvento.setMoradia(moradia);
		anuncioEvento.setCriador(criador);
		
		anuncioEventoRepository.save(anuncioEvento);
	}

	@Override
	public AnuncioEventoDto findById(Long id) {
		AnuncioEvento anuncioEvento = anuncioEventoRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(Mensagens.AnuncioEventoNaoEncontrado));
		return new AnuncioEventoDto(anuncioEvento);
	}

	@Override
	public PagedResponse<AnuncioEventoDto> findAll(Map<String, String> filters, int page, int size) {
		String cidade = filters.get("cidade");
		String estado = filters.get("estado");
				
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "nome");
		Page<AnuncioEvento> anuncioEventos;
		
		// Busca por cidade e estado
		if (cidade != null && estado != null) {
			anuncioEventos = anuncioEventoRepository.findByEstadoCidade(estado, cidade, pageable);			
		} else if (estado != null && cidade == null) {			
			anuncioEventos = anuncioEventoRepository.findByEstado(estado, pageable);			
		} else if(estado == null && cidade == null){
			anuncioEventos = anuncioEventoRepository.findAll(pageable);
		} else {
			throw new BadRequestException(Mensagens.EventoInformeEstado);
		}
		
		List<AnuncioEventoDto> anuncioEventosDto = new ArrayList<>();
        for (AnuncioEvento anuncio : anuncioEventos.getContent()) {
            anuncioEventosDto.add(new AnuncioEventoDto(anuncio));
        }

        PagedResponse<AnuncioEventoDto> pagedResponse = new PagedResponse<>(anuncioEventosDto, anuncioEventos.getNumber(), anuncioEventos.getSize(),
        		anuncioEventos.getTotalElements(), anuncioEventos.getTotalPages(), anuncioEventos.isLast());
        
        return pagedResponse;
	}
	
	@Override
	public List<AnuncioEventoDto> findByNome(String nome) {
		List<AnuncioEvento> anuncioEventos = anuncioEventoRepository.findByNomeContains(nome);
		List<AnuncioEventoDto> anuncioEventosDto = new ArrayList<>();
		
		for(AnuncioEvento a : anuncioEventos)
			anuncioEventosDto.add(new AnuncioEventoDto(a));
		
		return anuncioEventosDto;
	}
}
