package br.com.factio.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.factio.enums.Mensagens;
import br.com.factio.exception.BadRequestException;
import br.com.factio.exception.ResourceNotFoundException;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.ConcluirTarefaRequest;
import br.com.factio.payload.SalvarTarefaRequest;
import br.com.factio.payload.VincularResponsaveisRequest;
import br.com.factio.repository.MoradiaRepository;
import br.com.factio.repository.TarefaRepository;
import br.com.factio.repository.UsuarioRepository;
import br.com.factio.repository.entity.Moradia;
import br.com.factio.repository.entity.ResponsavelTarefa;
import br.com.factio.repository.entity.Tarefa;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.repository.entity.dto.TarefaDto;
import br.com.factio.security.CurrentUser;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.TarefaService;
import br.com.factio.service.UsuarioService;

@Service
public class TarefaServiceImpl implements TarefaService {
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private TarefaRepository tarefaRepository;

	@Autowired
	private MoradiaRepository moradiaRepository;

	@Transactional
	@Override
	public void salvarTarefa(UsuarioPrincipal usuarioLogado, SalvarTarefaRequest salvarTarefaRequest) {
		if (usuarioLogado.getUsuario().getMoradia().getId().compareTo(salvarTarefaRequest.getMoradiaId()) != 0)
			throw new ValidationException(Mensagens.Unauthorized);

		if (!validaDataConclusao(salvarTarefaRequest.getDataConclusao()))
			throw new ValidationException(Mensagens.DataConclusaoInvalida);

		Tarefa tarefa = new Tarefa();
		tarefa.setId(salvarTarefaRequest.getId());
		tarefa.setNome(salvarTarefaRequest.getNome());
		tarefa.setDescricao(salvarTarefaRequest.getDescricao());
		tarefa.setTipo(salvarTarefaRequest.getTipo());
		tarefa.setDataCriacao(Calendar.getInstance().getTime());

		Moradia moradia = moradiaRepository.findById(salvarTarefaRequest.getMoradiaId())
				.orElseThrow(() -> new ResourceNotFoundException(Mensagens.MoradiaNaoEncontrada));
		tarefa.setMoradia(moradia);

		try {
			tarefa = tarefaRepository.save(tarefa);
		} catch (DataIntegrityViolationException e) {
			throw new BadRequestException(Mensagens.TarefaExistente);
		}

		if (salvarTarefaRequest.getResponsaveis() != null) {
			for (Long l : salvarTarefaRequest.getResponsaveis()) {
				Usuario responsavel = usuarioService.findById(l);
				responsavel.addTarefa(tarefa, salvarTarefaRequest.getDataConclusao());
				usuarioRepository.saveAndFlush(responsavel);
			}
		}
	}

	@Override
	public TarefaDto findById(Long id) {
		Tarefa Tarefa = tarefaRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(Mensagens.TarefaNaoEncontrada));
		return new TarefaDto(Tarefa);
	}
	
	private Tarefa findTarefaById(Long id) {
		Tarefa tarefa = tarefaRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(Mensagens.TarefaNaoEncontrada));
		return tarefa;
	}

	@Override
	public List<TarefaDto> findByMoradia(Long moradiaId, @CurrentUser UsuarioPrincipal usuarioLogado) {
		Usuario usuario = usuarioLogado.getUsuario();
		Long idMoradia = usuario.getMoradia() != null ? usuario.getMoradia().getId() : null;

		// Cada usuário pode visualizar apenas tarefas da sua moradia
		if (idMoradia != null && idMoradia.compareTo(moradiaId) == 0) {
			List<Tarefa> tarefas = tarefaRepository.findByMoradiaId(moradiaId);
			List<TarefaDto> tarefasDto = new ArrayList<>();

			for (Tarefa t : tarefas)
				tarefasDto.add(new TarefaDto(t));

			return tarefasDto;
		} else {
			throw new BadRequestException(Mensagens.Unauthorized);
		}
	}

	@Override
	public List<TarefaDto> listarPorUsuarios(Long moradiaId, @CurrentUser UsuarioPrincipal usuarioLogado) {
		Usuario usuario = usuarioLogado.getUsuario();
		Long idMoradia = usuario.getMoradia() != null ? usuario.getMoradia().getId() : null;

		// Cada usuário pode visualizar apenas tarefas da sua moradia
		if (idMoradia != null && idMoradia.compareTo(moradiaId) == 0) {
			List<Tarefa> tarefas = tarefaRepository.findByMoradiaId(moradiaId);
			List<TarefaDto> tarefasDto = new ArrayList<>();

			for (Tarefa t : tarefas)
				tarefasDto.add(new TarefaDto(t, t.getUsuarios()));

			return tarefasDto;
		} else {
			throw new BadRequestException(Mensagens.Unauthorized);
		}
	}

	@Override
	public Page<Tarefa> findAllByExample(Map<String, String> filters, Pageable pageable) {
		// Tarefa Tarefa = new Tarefa();

		return null;
	}

	@Transactional
	@Override
	public void vincularResponsaveis(VincularResponsaveisRequest vincularResponsaveisRequest) {
		if (!validaDataConclusao(vincularResponsaveisRequest.getDataConclusao()))
			throw new ValidationException(Mensagens.DataConclusaoInvalida);

		Tarefa tarefa = tarefaRepository.findById(vincularResponsaveisRequest.getTarefaId())
				.orElseThrow(() -> new ResourceNotFoundException(Mensagens.TarefaNaoEncontrada));

		for (Long l : vincularResponsaveisRequest.getResponsaveis()) {
			Usuario responsavel = usuarioService.findById(l);
			for (ResponsavelTarefa t : responsavel.getTarefas()) {
				if (t.getId().getTarefaId().compareTo(tarefa.getId()) == 0)
					throw new BadRequestException(Mensagens.TarefaJaVinculada);
			}
			responsavel.addTarefa(tarefa, vincularResponsaveisRequest.getDataConclusao());
			usuarioRepository.saveAndFlush(responsavel);
		}
	}
	
	@Override
	public void concluirTarefa(ConcluirTarefaRequest concluirTarefaRequest) {
		Tarefa tarefa = this.findTarefaById(concluirTarefaRequest.getIdTarefa());
		
		for(ResponsavelTarefa rt : tarefa.getUsuarios()) {
			Usuario u = rt.getUsuario();
			for(ResponsavelTarefa resp : u.getTarefas()) {
				if(resp.getTarefa().getId().compareTo(concluirTarefaRequest.getIdTarefa()) == 0) {
					resp.setFinalizada(true);
					resp.setQualidadeTarefa(concluirTarefaRequest.getQualidadeTarefa());
					usuarioRepository.save(u);
					break;
				}
			}
		}		
	}
	
	@Override
	public void deletarTarefa(UsuarioPrincipal usuarioLogado, Long idTarefa) {
		Usuario usuario = usuarioLogado.getUsuario();
		Long idMoradia = usuario.getMoradia() != null ? usuario.getMoradia().getId() : null;
		Tarefa tarefa = this.findTarefaById(idTarefa);		
		
		// Cada usuário pode visualizar apenas tarefas da sua moradia
		if (idMoradia != null && idMoradia.compareTo(tarefa.getMoradia().getId()) == 0) {
			tarefaRepository.deleteById(idTarefa);
		} else {
			throw new BadRequestException(Mensagens.Unauthorized);		
		}
	}
	
	private boolean validaDataConclusao(Date dataConclusao) {
		Date dataAtual = Calendar.getInstance().getTime();

		return dataConclusao.after(dataAtual);
	}
}
