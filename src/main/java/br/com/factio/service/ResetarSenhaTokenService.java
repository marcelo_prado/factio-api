package br.com.factio.service;

import java.util.Date;

import org.springframework.stereotype.Service;

import br.com.factio.payload.RecuperarSenhaRequest;
import br.com.factio.repository.entity.ResetarSenhaToken;
import br.com.factio.repository.entity.Usuario;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */
@Service
public interface ResetarSenhaTokenService {
    void resetarSenhaToken(RecuperarSenhaRequest resetarSenhaToken);    
    void registraToken(ResetarSenhaToken resetarSenhaToken);
    void deletarTokensExpirados(Date date);
    void deletarToken(String token);
    ResetarSenhaToken findByUsuario(Usuario usuario);
    ResetarSenhaToken findByToken(String token);
    void validarToken(String token);
    
}
