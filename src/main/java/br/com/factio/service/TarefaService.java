package br.com.factio.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.factio.payload.ConcluirTarefaRequest;
import br.com.factio.payload.SalvarTarefaRequest;
import br.com.factio.payload.VincularResponsaveisRequest;
import br.com.factio.repository.entity.Tarefa;
import br.com.factio.repository.entity.dto.TarefaDto;
import br.com.factio.security.UsuarioPrincipal;

@Service
public interface TarefaService {
	void salvarTarefa(UsuarioPrincipal usuarioLogado, SalvarTarefaRequest salvarTarefaRequest);	

	TarefaDto findById(Long id);

	List<TarefaDto> findByMoradia(Long moradiaId, UsuarioPrincipal usuarioLogado);
	
	List<TarefaDto> listarPorUsuarios(Long moradiaId, UsuarioPrincipal usuarioLogado);

	Page<Tarefa> findAllByExample(Map<String, String> filters, Pageable pageable);	
	
	void vincularResponsaveis(VincularResponsaveisRequest vincularResponsaveisRequest);
	
	void concluirTarefa(ConcluirTarefaRequest concluirTarefaRequest);
	
	void deletarTarefa(UsuarioPrincipal usuarioLogado, Long idTarefa);
}
