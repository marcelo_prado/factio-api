package br.com.factio.service;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
public interface EmailService {
    public void sendEmail(SimpleMailMessage email);
    public void sendEmail(String to, String subject, String text);
}
