package br.com.factio.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.factio.payload.AtualizarMoradiaRequest;
import br.com.factio.payload.InserirMembroRequest;
import br.com.factio.payload.SalvarMoradiaRequest;
import br.com.factio.repository.entity.Moradia;
import br.com.factio.repository.entity.dto.MoradiaDto;
import br.com.factio.repository.entity.dto.SolicitacaoMoradiaDto;
import br.com.factio.repository.entity.dto.UsuarioDto;
import br.com.factio.security.UsuarioPrincipal;

@Service
public interface MoradiaService {
	void salvarMoradia(UsuarioPrincipal usuarioLogado, SalvarMoradiaRequest salvarMoradiaRequest);

	void atualizarMoradia(UsuarioPrincipal usuarioLogado, AtualizarMoradiaRequest atualizarMoradiaRequest);

	Moradia findById(Long id);

	List<Moradia> findAll();

	Page<Moradia> findAllByExample(Map<String, String> filters, Pageable pageable);
	
	void inserirMembro(InserirMembroRequest membro, UsuarioPrincipal usuarioLogado);
	
	void solicitarIngresso(InserirMembroRequest solicitacaoIngresso, UsuarioPrincipal usuarioLogado);
	
	Boolean removerSolicitacao(InserirMembroRequest removerSolicitacao, UsuarioPrincipal usuarioLogado);
	
	List<UsuarioDto> listarMembros(Long moradiaId, UsuarioPrincipal usuarioLogado);
	
	List<SolicitacaoMoradiaDto> listarSolicitacoes(Long moradiaId, UsuarioPrincipal usuarioLogado);
	
	MoradiaDto listarMoradiaPorMembro(Long idMembro, UsuarioPrincipal usuarioLogado);
}
