package br.com.factio.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import br.com.factio.payload.PagedResponse;
import br.com.factio.payload.SalvarAnuncioEventoRequest;
import br.com.factio.repository.entity.dto.AnuncioEventoDto;
import br.com.factio.security.UsuarioPrincipal;

@Service
public interface AnuncioEventoService {
	void salvarAnuncioEvento(UsuarioPrincipal usuarioLogado, SalvarAnuncioEventoRequest salvarAnuncioEventoRequest);	

	AnuncioEventoDto findById(Long id);
	
	List<AnuncioEventoDto> findByNome(String nome);
	
	PagedResponse<AnuncioEventoDto> findAll(Map<String, String> filters, int page, int size);
	
}
