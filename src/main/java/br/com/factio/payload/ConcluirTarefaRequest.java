package br.com.factio.payload;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConcluirTarefaRequest {	
	@NotNull
	private Long idTarefa;
		
	private Integer qualidadeTarefa;
}
