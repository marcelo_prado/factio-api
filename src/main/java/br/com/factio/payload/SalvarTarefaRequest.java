package br.com.factio.payload;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalvarTarefaRequest {	
    private Long id;

    @NotNull
    @Size(max = 60)    
    private String nome;
    
    @NotNull
    @Size(max = 200)
    private String descricao;
    
    @NotNull
    @Size(max = 50)
    private String tipo;
    
    @NotNull
    private Date dataConclusao;
    
    @NotNull
	private Long moradiaId;
    
    private List<Long> responsaveis;
}
