package br.com.factio.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest {

    @NotBlank
    @Email
    @Getter
    private String email;

    @NotBlank
    @Getter
    private String senha;
}
