package br.com.factio.payload;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalvarAnuncioEventoRequest {
	private Long id;
			
	@NotNull
	@Size(max = 45)
	private String nome;
	
	@NotNull
	@Size(max = 100)
	private String descricao;
	
	@NotNull
	@Size(max = 13)	
	private String telefoneContato;
	
	@NotNull
	@Size(max = 1000)
	private String atracoes;
	
	@NotNull
	private Date dataRealizacao;
	
	@NotNull
	private BigDecimal valorEntrada;
	
	@NotNull
	private Long moradiaId;
	
	@NotNull
	private Long criadorId;
}
