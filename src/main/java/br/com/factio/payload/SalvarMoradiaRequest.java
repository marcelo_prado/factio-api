package br.com.factio.payload;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalvarMoradiaRequest {
	@Size(max = 30)
	@NotNull
	private String apelido;
	
	@Size(max = 40)
	@NotNull
	private String nome;
	
	@Size(max = 80)
	@NotNull
	private String endereco;
	
	@Column(name = "numero")
	@NotNull
	private int numero;
	
	@Size(max = 40)
	private String complemento;
	
	@Size(max = 40)
	@NotNull
	private String bairro;
	
	@Size(max = 40)
	@NotNull
	private String cidade;
	
	@Size(max = 2)
	@NotNull
	private String estado;
	
	@Size(max = 10)
	@NotNull
	private String cep;
	
	@Size(max = 200)
	private String comentario;
	
	@NotNull
	private Long responsavel;
}
