package br.com.factio.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecuperarSenhaRequest {
    private String email;
}
