package br.com.factio.payload;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PagedResponse<T extends Serializable> implements Serializable {
	private static final long serialVersionUID = -5643276493915701319L;
	
	private List<T> content;
	private int page;
	private int size;
	private long totalElements;
	private int totalPages;
	private boolean last;
}
