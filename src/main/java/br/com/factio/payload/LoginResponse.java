package br.com.factio.payload;

import java.io.Serializable;

import br.com.factio.repository.entity.dto.UsuarioDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse implements Serializable {

    private static final long serialVersionUID = 203931099964032604L;

    private String accessToken;
    private UsuarioDto usuario;

}
