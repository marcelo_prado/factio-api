package br.com.factio.payload;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlterarSenhaTokenRequest {
    @NotBlank
    private String token;

    @NotBlank
    private String senhaNova;
}
