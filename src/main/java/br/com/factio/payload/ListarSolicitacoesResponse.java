package br.com.factio.payload;

import java.io.Serializable;
import java.util.List;

import br.com.factio.repository.entity.dto.SolicitacaoMoradiaDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ListarSolicitacoesResponse implements Serializable {
	private static final long serialVersionUID = -1748006908163597218L;
	
	private List<SolicitacaoMoradiaDto> solicitacoes;
}
