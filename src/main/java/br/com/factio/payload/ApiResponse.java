package br.com.factio.payload;

import java.io.Serializable;

import br.com.factio.enums.Mensagens;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@Data
@AllArgsConstructor
public class ApiResponse<T extends Serializable> implements Serializable {

	private static final long serialVersionUID = 8316115263971253201L;

	private MetaData meta;
	private T data;

	public ApiResponse(MetaData meta) {
		this.meta = meta;
	}

	public ApiResponse(Mensagens key) {
		this.meta = new MetaData(key);
	}

	public ApiResponse(Mensagens key, T data) {
		this.meta = new MetaData(key);
		this.data = data;
	}

	public ApiResponse(String messageError, String messageDescription, String path, T data) {
		this.meta = new MetaData(messageError, messageDescription, path);
		this.data = data;
	}

	public ApiResponse(String messageError, String messageDescription, String path) {
		this.meta = new MetaData(messageError, messageDescription, path);
	}
}
