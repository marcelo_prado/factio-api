package br.com.factio.payload;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalvarUsuarioRequest {
    @NotNull
    @Size(max = 100)
    private String nome;
    
    @NotNull
    @Size(max = 13)
    @Column(name = "telefone")
    private String telefone;

    @Email
    @NotNull
    @Size(max = 100)
    private String email;
    
    @NotNull
    @Size(max = 20)
    @Column(name = "senha")
    private String senha;

    @NotNull
    private boolean ativo;

    @NotNull
    private Long perfil;
}
