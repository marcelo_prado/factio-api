package br.com.factio.payload;

import java.io.Serializable;
import java.util.List;

import br.com.factio.repository.entity.dto.AnuncioVagaDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListarAnuncioVagasResponse implements Serializable{
	private static final long serialVersionUID = -3709634181922850256L;
	
	private List<AnuncioVagaDto> anuncioVagasDto;
}
