package br.com.factio.payload;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalvarAnuncioVagaRequest {
	private Long id;
	
	@NotNull
	private Integer qtdQuartos;

	@NotNull
	private String descricao;
	
	@Size(max = 50)	
	@NotNull
	private String publicoAlvo;

	@NotNull
	private String observacoes;
		
	@NotNull	
	private Long moradiaId;
	
	@NotNull
	private Long criadorId;
}
