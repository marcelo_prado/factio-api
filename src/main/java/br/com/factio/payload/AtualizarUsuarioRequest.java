package br.com.factio.payload;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AtualizarUsuarioRequest {
    @NotNull
    private Long id;

    @NotNull
    @Size(max = 100)
    private String nome;    

    @NotNull
    @Size(max = 13)
    private String telefone;

    @NotNull
    private boolean ativo;

    @NotNull
    private Long perfil;

    private Date ultimoLogin;
}
