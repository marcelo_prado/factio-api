package br.com.factio.payload;

import java.io.Serializable;

import br.com.factio.enums.Mensagens;
import br.com.factio.util.RestControllerUtil;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * @author Grupo 2 - PDSI 
 *
 */
@Data
@AllArgsConstructor
public class MetaData implements Serializable {

	private static final long serialVersionUID = 724057721217352098L;

	private String messageCode = "SUCCESS";
	private String messageDescription;
	private String path = RestControllerUtil.getPathFromContext();

	public MetaData(String messageCode, String messageDescription) {
		super();
		this.messageCode = messageCode;
		this.messageDescription = messageDescription;
	}

	public MetaData(Mensagens mensagem) {
		super();
		this.messageDescription = mensagem.getValue();
		this.messageCode = mensagem.getKey();
	}
}
