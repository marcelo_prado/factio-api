package br.com.factio.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public abstract class EmailDisponivelRequest {
	
	@NotBlank
	@Email
	@Getter
	private String email;
	
}
