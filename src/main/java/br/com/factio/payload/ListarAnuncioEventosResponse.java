package br.com.factio.payload;

import java.io.Serializable;
import java.util.List;

import br.com.factio.repository.entity.dto.AnuncioEventoDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListarAnuncioEventosResponse implements Serializable{
	
	private static final long serialVersionUID = -6613815613075481176L;
	
	private List<AnuncioEventoDto> anuncioEventos;
}
