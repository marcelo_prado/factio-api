package br.com.factio.payload;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class InserirMembroRequest {	
	@NotNull
	private Long usuario;
	
	@NotNull
	private Long moradia;
}
