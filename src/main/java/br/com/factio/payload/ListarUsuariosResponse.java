package br.com.factio.payload;

import java.io.Serializable;
import java.util.List;

import br.com.factio.repository.entity.dto.UsuarioDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListarUsuariosResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -224175341431796395L;
	
	private List<UsuarioDto> usuarios;
}
