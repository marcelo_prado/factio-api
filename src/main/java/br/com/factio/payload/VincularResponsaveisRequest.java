package br.com.factio.payload;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VincularResponsaveisRequest {
	@NotNull
	private Long tarefaId;
	
	@NotNull
	@NotEmpty
	private List<Long> responsaveis;
	
	@NotNull
	private Date dataConclusao;
}
