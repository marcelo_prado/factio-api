package br.com.factio.payload;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionResponse implements Serializable {	
	private static final long serialVersionUID = -1971467612315076759L;
	
	private String cause;
	private String message;
}
