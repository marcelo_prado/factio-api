package br.com.factio.payload;

import java.io.Serializable;
import java.util.List;

import br.com.factio.repository.entity.dto.TarefaDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListarTarefasResponse implements Serializable{
	private static final long serialVersionUID = -1063200437012669045L;

	private List<TarefaDto> tarefas;
}
