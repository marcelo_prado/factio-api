package br.com.factio.payload;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalvarLocalizacaoRequest {
	@NotNull
	private Long usuarioId;
	
	@Size(max = 30)
	@NotNull
	private String latitude;
	
	@Size(max = 30)
	@NotNull
	private String longitude;
	
	@NotNull
	private Date horaLocalizacao;
}
