package br.com.factio.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.factio.enums.Mensagens;
import br.com.factio.payload.ApiResponse;
import br.com.factio.payload.PagedResponse;
import br.com.factio.payload.SalvarAnuncioVagaRequest;
import br.com.factio.repository.entity.dto.AnuncioVagaDto;
import br.com.factio.security.CurrentUser;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.AnuncioVagaService;
import br.com.factio.util.AppConstants;

@RestController
@RequestMapping("/api/vagas")
public class AnuncioVagaController {
	@Autowired
	private AnuncioVagaService anuncioVagaService;
	
	@PostMapping
	public ResponseEntity<?> salvarAnuncioVaga(@CurrentUser UsuarioPrincipal usuarioLogado, 
			@Valid @RequestBody SalvarAnuncioVagaRequest salvarAnuncioVagaRequest) { 
		anuncioVagaService.salvarAnuncioVaga(usuarioLogado, salvarAnuncioVagaRequest);

		Mensagens msg = salvarAnuncioVagaRequest.getId() != null 
				? Mensagens.AnuncioVagaAtualizado : Mensagens.AnuncioVagaRegistrado;
		
        ApiResponse<?> response = new ApiResponse<>(msg);

        return ResponseEntity.ok(response);
	}	
	
	@GetMapping
	public ResponseEntity<?> listarAnuncioVagas(@CurrentUser UsuarioPrincipal usuarioLogado, 
			@RequestParam(required = false) Map<String, String> filters,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) { 
		
		PagedResponse<AnuncioVagaDto> anuncioVagas = anuncioVagaService.findAll(filters, page, size);
		
        ApiResponse<?> response = 
        		new ApiResponse<PagedResponse<AnuncioVagaDto>>(Mensagens.ServicoExecutadoSucesso, anuncioVagas);

        return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> listarAnuncioVaga(@CurrentUser UsuarioPrincipal usuarioLogado, @PathVariable Long id) { 
		AnuncioVagaDto anuncioVaga = anuncioVagaService.findById(id);
				
        ApiResponse<?> response = new ApiResponse<AnuncioVagaDto>(Mensagens.ServicoExecutadoSucesso, anuncioVaga);

        return ResponseEntity.ok(response);
	}
}
