package br.com.factio.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.factio.enums.Mensagens;
import br.com.factio.payload.ApiResponse;
import br.com.factio.payload.ConcluirTarefaRequest;
import br.com.factio.payload.DeletarTarefaRequest;
import br.com.factio.payload.ListarTarefasResponse;
import br.com.factio.payload.SalvarTarefaRequest;
import br.com.factio.payload.VincularResponsaveisRequest;
import br.com.factio.repository.entity.dto.TarefaDto;
import br.com.factio.security.CurrentUser;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.TarefaService;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping("/api/tarefas")
public class TarefaController {
	@Autowired
	private TarefaService tarefaService;
	
	@PostMapping
	public ResponseEntity<?> salvarTarefa(@CurrentUser UsuarioPrincipal usuarioLogado, 
			@Valid @RequestBody SalvarTarefaRequest salvarTarefaRequest) { 
		tarefaService.salvarTarefa(usuarioLogado, salvarTarefaRequest);

		Mensagens msg = salvarTarefaRequest.getId() != null 
				? Mensagens.TarefaAtualizada : Mensagens.TarefaRegistrada;
		
        ApiResponse<?> response = new ApiResponse<>(msg);

        return ResponseEntity.ok(response);
	}	
	
	@GetMapping
	public ResponseEntity<?> listarTarefas(@RequestParam(value = "moradiaId") Long moradiaId, @CurrentUser UsuarioPrincipal usuarioLogado) { 
		List<TarefaDto> tarefas = tarefaService.listarPorUsuarios(moradiaId, usuarioLogado);
		ListarTarefasResponse listarEventos = new ListarTarefasResponse(tarefas);
		
        ApiResponse<?> response = new ApiResponse<ListarTarefasResponse>(Mensagens.ServicoExecutadoSucesso, listarEventos);

        return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> listarTarefa(@CurrentUser UsuarioPrincipal usuarioLogado, @PathVariable Long id) { 
		TarefaDto tarefa = tarefaService.findById(id);
				
        ApiResponse<?> response = new ApiResponse<TarefaDto>(Mensagens.ServicoExecutadoSucesso, tarefa);

        return ResponseEntity.ok(response);
	}
	
	
	@PostMapping("/vincularResponsaveis")
	public ResponseEntity<?> vincularResponsaveis(@CurrentUser UsuarioPrincipal usuarioLogado, 
			@RequestBody @Valid VincularResponsaveisRequest vincularResponsaveisRequest){
		tarefaService.vincularResponsaveis(vincularResponsaveisRequest);
		
		ApiResponse<?> response = new ApiResponse(Mensagens.ServicoExecutadoSucesso);

        return ResponseEntity.ok(response);
	}
	
	@PostMapping("/concluirTarefa")
	public ResponseEntity<?> concluirTarefa(@CurrentUser UsuarioPrincipal usuarioLogado, 
			@RequestBody @Valid ConcluirTarefaRequest concluirTarefaRequest){
		tarefaService.concluirTarefa(concluirTarefaRequest);
		
		ApiResponse<?> response = new ApiResponse(Mensagens.TarefaConcluida);

        return ResponseEntity.ok(response);
	}
	
	@DeleteMapping
	public ResponseEntity<?> deletarTarefa(@CurrentUser UsuarioPrincipal usuarioLogado, 
			@RequestBody @Valid DeletarTarefaRequest deletarRequest) {
		tarefaService.deletarTarefa(usuarioLogado, deletarRequest.getTarefaId());
		
		ApiResponse<?> response = new ApiResponse(Mensagens.TarefaDeletada);

        return ResponseEntity.ok(response);
	}
}
