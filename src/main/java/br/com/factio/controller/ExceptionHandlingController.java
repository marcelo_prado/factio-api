package br.com.factio.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.factio.enums.Mensagens;
import br.com.factio.exception.BadRequestException;
import br.com.factio.exception.ResourceNotFoundException;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.ApiResponse;
import br.com.factio.payload.ExceptionResponse;

@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    ApiResponse<?> handleBadRequest(HttpServletRequest req, Exception ex) {
        BadRequestException e = (BadRequestException) ex;
        return new ApiResponse<>(e.getKey(), e.getValue(), req.getRequestURI().toString());
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(BadCredentialsException.class)
    @ResponseBody
    ApiResponse<?> handleBadCredentialsRequest(HttpServletRequest req, Exception ex) {  
    	return new ApiResponse<>(Mensagens.BadCredentials);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseBody
    ApiResponse<?> handleNotFound(HttpServletRequest req, Exception ex) {
        ResourceNotFoundException e = (ResourceNotFoundException) ex;
        return new ApiResponse<>(e.getKey(), e.getValue(), req.getRequestURI().toString());
    }

    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(ValidationException.class)
    @ResponseBody
    ApiResponse<?> handleValidationException(HttpServletRequest req, Exception ex) {
        ValidationException e = (ValidationException) ex;
        return new ApiResponse<>(e.getKey(), e.getValue(), req.getRequestURI().toString());
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(AuthenticationException.class)
    @ResponseBody
    ApiResponse<?> handleUnauthorized(HttpServletRequest req, Exception ex) {
        return new ApiResponse<>(Mensagens.Unauthorized);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    ApiResponse<?> handleInternalServerError(HttpServletRequest req, Exception ex) {
        ExceptionResponse errorBody = new ExceptionResponse();
        if (ex.getCause() != null) {
            errorBody.setCause(ex.getCause().getMessage());
        }

        errorBody.setMessage(ex.getMessage());
        return new ApiResponse<>(Mensagens.InternalServerError, errorBody);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
        HttpStatus status, WebRequest request) {

        ExceptionResponse errorBody = new ExceptionResponse();
        if (ex.getCause() != null) {
            errorBody.setCause(ex.getCause().getMessage());
        }

        errorBody.setMessage(ex.getMessage());

        ApiResponse<ExceptionResponse> response = new ApiResponse<>(Mensagens.InternalServerError, errorBody);
        return ResponseEntity.status(status).body(response);
    }
}
