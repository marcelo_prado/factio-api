package br.com.factio.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.factio.enums.Mensagens;
import br.com.factio.payload.ApiResponse;
import br.com.factio.payload.AtualizarUsuarioRequest;
import br.com.factio.payload.LoginRequest;
import br.com.factio.payload.LoginResponse;
import br.com.factio.payload.SalvarUsuarioRequest;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.repository.entity.dto.UsuarioDto;
import br.com.factio.security.CurrentUser;
import br.com.factio.security.JwtTokenProvider;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.UsuarioService;

@RestController
@RequestMapping(path = "/api/auth")
public class AuthController {

	@Autowired
	private AuthenticationManager authenticationManager;

//	@Autowired
//	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider tokenProvider;

	@Autowired
	private UsuarioService usuarioService;

	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getSenha()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = tokenProvider.generateToken(authentication);

		UsuarioPrincipal principal = (UsuarioPrincipal) authentication.getPrincipal();

		Usuario usuario = principal.getUsuario();

		UsuarioDto usuarioDto = new UsuarioDto(usuario);

		usuarioService.atualizarUsuario(principal, new AtualizarUsuarioRequest(usuario.getId(), usuario.getNome(), usuario.getTelefone(),
				usuario.getAtivo(), usuario.getPerfil().getId(), new Date()));

		ApiResponse<LoginResponse> response = new ApiResponse<>(Mensagens.ServicoExecutadoSucesso,
				new LoginResponse(jwt, usuarioDto));

		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/signin")
    public ResponseEntity<?> salvar(@CurrentUser UsuarioPrincipal usuarioLogado,
        @Valid @RequestBody SalvarUsuarioRequest salvarUsuarioRequest) {
        this.usuarioService.salvarUsuario(salvarUsuarioRequest);

        ApiResponse<?> response = new ApiResponse<>(Mensagens.UsuarioRegistrado);

        return ResponseEntity.ok(response);
    }
}
