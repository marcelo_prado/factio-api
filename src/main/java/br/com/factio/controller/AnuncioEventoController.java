package br.com.factio.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.factio.enums.Mensagens;
import br.com.factio.payload.ApiResponse;
import br.com.factio.payload.ListarAnuncioEventosResponse;
import br.com.factio.payload.PagedResponse;
import br.com.factio.payload.SalvarAnuncioEventoRequest;
import br.com.factio.repository.entity.dto.AnuncioEventoDto;
import br.com.factio.security.CurrentUser;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.AnuncioEventoService;
import br.com.factio.util.AppConstants;

@RestController
@RequestMapping("/api/eventos")
public class AnuncioEventoController {
	@Autowired
	private AnuncioEventoService anuncioEventoService;
	
	@PostMapping
	public ResponseEntity<?> salvarAnuncioEvento(@CurrentUser UsuarioPrincipal usuarioLogado, 
			@Valid @RequestBody SalvarAnuncioEventoRequest salvarAnuncioEventoRequest) { 
		anuncioEventoService.salvarAnuncioEvento(usuarioLogado, salvarAnuncioEventoRequest);

		Mensagens msg = salvarAnuncioEventoRequest.getId() != null 
				? Mensagens.AnuncioEventoAtualizado : Mensagens.AnuncioEventoRegistrado;
		
        ApiResponse<?> response = new ApiResponse<>(msg);

        return ResponseEntity.ok(response);
	}	
	
	@GetMapping
	public ResponseEntity<?> listarAnuncioEventos(@CurrentUser UsuarioPrincipal usuarioLogado, 
			@RequestParam(required = false) Map<String, String> filters,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) { 
		
		PagedResponse<AnuncioEventoDto> anuncioEventos = anuncioEventoService.findAll(filters, page, size);
		
        ApiResponse<?> response = 
        		new ApiResponse<PagedResponse<AnuncioEventoDto>>(Mensagens.ServicoExecutadoSucesso, anuncioEventos);

        return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> listarAnuncioEventoId(@CurrentUser UsuarioPrincipal usuarioLogado, @PathVariable Long id) { 
		AnuncioEventoDto anuncioEvento = anuncioEventoService.findById(id);
				
        ApiResponse<?> response = new ApiResponse<AnuncioEventoDto>(Mensagens.ServicoExecutadoSucesso, anuncioEvento);

        return ResponseEntity.ok(response);
	}
	
	@GetMapping("/nome/{nome}")
	public ResponseEntity<?> listarAnuncioEventoNome(@CurrentUser UsuarioPrincipal usuarioLogado, @PathVariable String nome) { 
		List<AnuncioEventoDto> anuncioEventos = anuncioEventoService.findByNome(nome);
		ListarAnuncioEventosResponse listaAnuncios = new ListarAnuncioEventosResponse(anuncioEventos);	
		
        ApiResponse<?> response = new ApiResponse<ListarAnuncioEventosResponse>(Mensagens.ServicoExecutadoSucesso, listaAnuncios);

        return ResponseEntity.ok(response);
	}
}
