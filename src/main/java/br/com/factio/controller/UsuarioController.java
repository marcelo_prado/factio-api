package br.com.factio.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.factio.enums.Mensagens;
import br.com.factio.payload.AlterarSenhaRequest;
import br.com.factio.payload.AlterarSenhaTokenRequest;
import br.com.factio.payload.ApiResponse;
import br.com.factio.payload.AtualizarUsuarioRequest;
import br.com.factio.payload.DisponibilidadeResponse;
import br.com.factio.payload.PagedResponse;
import br.com.factio.payload.RecuperarSenhaRequest;
import br.com.factio.payload.SalvarLocalizacaoRequest;
import br.com.factio.repository.entity.dto.UsuarioDto;
import br.com.factio.security.CurrentUser;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.ResetarSenhaTokenService;
import br.com.factio.service.UsuarioService;
import br.com.factio.util.AppConstants;

/**
 * 
 * @author Grupo 2 - PDSI
 * 
 */
@SuppressWarnings("rawtypes")
@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private ResetarSenhaTokenService resetarSenhaTokenService;

	@GetMapping("/me")
	public ResponseEntity<?> me(@CurrentUser UsuarioPrincipal usuarioLogado) {
		UsuarioDto dto = new UsuarioDto(usuarioLogado.getUsuario());
		ApiResponse<UsuarioDto> response = new ApiResponse<>(Mensagens.ServicoExecutadoSucesso, dto);
		return ResponseEntity.ok(response);
	}

	@GetMapping("/verificarEmail")
	public ResponseEntity<?> checkEmailAvailability(@RequestParam(value = "email", required = true) String email) {
		boolean isPresent = usuarioService.existsByEmail(email);

		DisponibilidadeResponse<String> disponibilidadeResponse = new DisponibilidadeResponse<String>(email,
				!isPresent);
		ApiResponse<DisponibilidadeResponse> response = new ApiResponse<>(Mensagens.ServicoExecutadoSucesso,
				disponibilidadeResponse);
		return ResponseEntity.ok(response);
	}

	// @PreAuthorize("hasRole('ROLE_USUARIO')")
	@PutMapping
	public ResponseEntity<?> atualizar(@CurrentUser UsuarioPrincipal usuarioLogado,
			@Valid @RequestBody AtualizarUsuarioRequest atualizarUsuarioRequest) {
		this.usuarioService.atualizarUsuario(usuarioLogado, atualizarUsuarioRequest);

		ApiResponse<?> response = new ApiResponse<>(Mensagens.UsuarioAtualizado);

		return ResponseEntity.ok(response);
	}

	@PostMapping("/alterarSenha")
	public ResponseEntity<?> alterarSenha(@CurrentUser UsuarioPrincipal usuarioLogado,
			@Valid @RequestBody AlterarSenhaRequest alterarSenhaRequest) {

		usuarioService.alterarSenha(usuarioLogado, alterarSenhaRequest);
		ApiResponse<?> response = new ApiResponse<>(Mensagens.SenhaAlterada);
		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/salvarLocalizacao")
	public ResponseEntity<?> salvarLocalizacao(@CurrentUser UsuarioPrincipal usuarioLogado,
			@Valid @RequestBody SalvarLocalizacaoRequest salvarLocalizacaoRequest) {

		usuarioService.salvarLocalizacao(usuarioLogado, salvarLocalizacaoRequest);
		ApiResponse<?> response = new ApiResponse<>(Mensagens.LocalizacaoSalva);
		return ResponseEntity.ok(response);
	}

	@PostMapping("/alterarSenhaToken")
	public ResponseEntity<?> alterarSenhaToken(@Valid @RequestBody AlterarSenhaTokenRequest alterarSenhaTokenRequest) {
		usuarioService.alterarSenhaToken(alterarSenhaTokenRequest);
		ApiResponse<?> response = new ApiResponse<>(Mensagens.SenhaAlterada);
		return ResponseEntity.ok(response);
	}

	@PostMapping("/recuperarSenha")
	public ResponseEntity<?> recuperarSenha(@Valid @RequestBody RecuperarSenhaRequest recuperarSenhaRequest) {
		resetarSenhaTokenService.resetarSenhaToken(recuperarSenhaRequest);
		ApiResponse<?> response = new ApiResponse<>(Mensagens.TokenRegistradoSucesso);
		return ResponseEntity.ok(response);
	}

	@GetMapping("/recuperarSenha")
	public ResponseEntity<?> recuperarSenha(@RequestParam(value = "token", required = true) String token) {
		resetarSenhaTokenService.validarToken(token);
		ApiResponse<?> response = new ApiResponse<>(Mensagens.TokenValido);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	@GetMapping
	public ResponseEntity<?> listarUsuarios(@RequestParam(required = false) Map<String, String> filters,
			@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
			@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
		filters.remove("page");
		filters.remove("size");

		PagedResponse<UsuarioDto> pagedResponse = usuarioService.findAllByExample(filters, page, size);

		ApiResponse<?> response = new ApiResponse<PagedResponse>(Mensagens.ServicoExecutadoSucesso, pagedResponse);
		return ResponseEntity.ok(response);
	}

	@PreAuthorize("hasRole('ROLE_ADMINISTRADOR')")
	@GetMapping("/{id}")
	public ResponseEntity<?> listarUsuario(@CurrentUser UsuarioPrincipal usuarioLogado, @PathVariable Long id) {
		UsuarioDto usuario = new UsuarioDto(usuarioService.findById(id));
		
		ApiResponse<?> response = new ApiResponse<UsuarioDto>(Mensagens.ServicoExecutadoSucesso, usuario);

		return ResponseEntity.ok(response);
	}

}
