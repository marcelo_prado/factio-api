package br.com.factio.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.factio.enums.Mensagens;
import br.com.factio.payload.ApiResponse;
import br.com.factio.payload.AtualizarMoradiaRequest;
import br.com.factio.payload.InserirMembroRequest;
import br.com.factio.payload.ListarSolicitacoesResponse;
import br.com.factio.payload.ListarUsuariosResponse;
import br.com.factio.payload.PagedResponse;
import br.com.factio.payload.SalvarMoradiaRequest;
import br.com.factio.repository.entity.Moradia;
import br.com.factio.repository.entity.dto.MoradiaDto;
import br.com.factio.repository.entity.dto.SolicitacaoMoradiaDto;
import br.com.factio.repository.entity.dto.UsuarioDto;
import br.com.factio.security.CurrentUser;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.MoradiaService;
import br.com.factio.util.AppConstants;

@SuppressWarnings("rawtypes")
@RestController
@RequestMapping("/api/moradias")
public class MoradiaController {
	
	@Autowired
    private MoradiaService moradiaService;
    
	//@PreAuthorize("hasRole('ROLE_USUARIO')")
    @PostMapping
    public ResponseEntity<?> salvar(@CurrentUser UsuarioPrincipal usuarioLogado,
        @Valid @RequestBody SalvarMoradiaRequest salvarMoradiaRequest) {
        moradiaService.salvarMoradia(usuarioLogado, salvarMoradiaRequest);

        ApiResponse<?> response = new ApiResponse<>(Mensagens.MoradiaRegistrada);

        return ResponseEntity.ok(response);
    }

    //@PreAuthorize("hasRole('ROLE_USUARIO')")
    @PutMapping
    public ResponseEntity<?> atualizar(@CurrentUser UsuarioPrincipal usuarioLogado,
        @Valid @RequestBody AtualizarMoradiaRequest atualizarMoradiaRequest) {
    	moradiaService.atualizarMoradia(usuarioLogado, atualizarMoradiaRequest);
    	
    	ApiResponse<?> response = new ApiResponse<>(Mensagens.MoradiaAtualizada);
    	
        return ResponseEntity.ok(response);
    }
    
    @PostMapping("/inserirMembro")
    public ResponseEntity<?> inserirMembro(@RequestBody InserirMembroRequest inserirMembroRequest, @CurrentUser UsuarioPrincipal usuarioLogado) {
    	moradiaService.inserirMembro(inserirMembroRequest, usuarioLogado);
    	
    	ApiResponse<?> response = new ApiResponse(Mensagens.InserirUsuarioMoradia);
    	return ResponseEntity.ok(response);
    }
    
    @DeleteMapping("/removerSolicitacao")
    public ResponseEntity<?> removerSolicitacao(@RequestBody InserirMembroRequest removerSolicitacao, @CurrentUser UsuarioPrincipal usuarioLogado) {
    	Boolean sucesso = moradiaService.removerSolicitacao(removerSolicitacao, usuarioLogado);
    	
    	ApiResponse<?> response = new ApiResponse(sucesso ? Mensagens.SolicitacaoRemovida : Mensagens.ServicoExecutadoSucesso);
    	return ResponseEntity.ok(response);
    }
    
    @PostMapping("/solicitacaoIngresso")
    public ResponseEntity<?> criarSolicitacaoIngresso(@RequestBody InserirMembroRequest solicitarIngressoRequest, @CurrentUser UsuarioPrincipal usuarioLogado) {
    	moradiaService.solicitarIngresso(solicitarIngressoRequest, usuarioLogado);
    	
    	ApiResponse<?> response = new ApiResponse(Mensagens.SolicitacaoEnviada);
    	return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasRole('ROLE_USUARIO')")
    @GetMapping
    public ResponseEntity<?> listarMoradias(@RequestParam(required = false) Map<String, String> filters,
        @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
        @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        filters.remove("page");
        filters.remove("size");
        
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "nome");
        Page<Moradia> moradias = moradiaService.findAllByExample(filters, pageable);

        List<MoradiaDto> moradiasDto = new ArrayList<MoradiaDto>();
        for (Moradia m : moradias.getContent()) {
            moradiasDto.add(new MoradiaDto(m));
        }

        PagedResponse pagedResponse = new PagedResponse<>(moradiasDto, moradias.getNumber(), moradias.getSize(),
            moradias.getTotalElements(), moradias.getTotalPages(), moradias.isLast());

        ApiResponse<?> response = new ApiResponse<PagedResponse>(Mensagens.ServicoExecutadoSucesso, pagedResponse);
        return ResponseEntity.ok(response);
    }
    
    @GetMapping("/membros")
    public ResponseEntity<?> listarMembros(@RequestParam(value = "moradiaId") Long moradiaId, @CurrentUser UsuarioPrincipal usuarioLogado) {       
        List<UsuarioDto> usuarios = moradiaService.listarMembros(moradiaId, usuarioLogado);
        ListarUsuariosResponse listaUsuarios = new ListarUsuariosResponse();
        listaUsuarios.setUsuarios(usuarios);
        
        ApiResponse<?> response = new ApiResponse<>(Mensagens.ServicoExecutadoSucesso, listaUsuarios);
        return ResponseEntity.ok(response);
    }
    
    @GetMapping("/solicitacoes/{id}")
    public ResponseEntity<?> listarSolicitacoes(@CurrentUser UsuarioPrincipal usuarioLogado, @PathVariable Long id) {       
        List<SolicitacaoMoradiaDto> solicitacoes = moradiaService.listarSolicitacoes(id, usuarioLogado);
        ListarSolicitacoesResponse listaSolicitacoes = new ListarSolicitacoesResponse();
        listaSolicitacoes.setSolicitacoes(solicitacoes);
        
        ApiResponse<?> response = new ApiResponse<>(Mensagens.ServicoExecutadoSucesso, listaSolicitacoes);
        return ResponseEntity.ok(response);
    }
    
    @GetMapping("/membro/{id}")
    public ResponseEntity<?> listarPorMembro(@CurrentUser UsuarioPrincipal usuarioLogado, @PathVariable Long id) {       
        MoradiaDto moradia = moradiaService.listarMoradiaPorMembro(id, usuarioLogado);
        
        ApiResponse<?> response = new ApiResponse<>(Mensagens.ServicoExecutadoSucesso, moradia);
        return ResponseEntity.ok(response);
    }
}
