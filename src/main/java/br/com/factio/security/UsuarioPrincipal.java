package br.com.factio.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.factio.repository.entity.Usuario;
import lombok.Getter;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
public class UsuarioPrincipal implements UserDetails {

	private static final long serialVersionUID = 4352006737105711671L;
	
	@Getter 
	private Usuario usuario;
	
	private Collection<? extends GrantedAuthority> authorities;
	
	public UsuarioPrincipal(Usuario usuario, Collection<? extends GrantedAuthority> authorities) {
		this.usuario = usuario;
		this.authorities = authorities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return usuario.getSenha();
	}

	@Override
	public String getUsername() {
		return usuario.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return usuario.getAtivo();
	}

	
}
