package br.com.factio.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.factio.repository.entity.Role;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.service.UsuarioService;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@Service
public class UsuarioDetailService implements UserDetailsService {
    
    @Autowired
    private UsuarioService usuarioService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String emailUsuario)
            throws UsernameNotFoundException {
    	// usuário irá realizar login por e-mail.
        Usuario user = usuarioService.findByEmail(emailUsuario);

        return new UsuarioPrincipal(user, getAuthorities(user));
    }

    @Transactional
    public UserDetails loadUserById(final Long id) {
        Usuario user = usuarioService.findById(id);
		
        return new UsuarioPrincipal(user, getAuthorities(user));
    }
    
    @Transactional
    public List<GrantedAuthority> getAuthorities(Usuario usuario) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        List<Role> roles = this.usuarioService.consultarPermissoesPorIdPerfil(usuario.getPerfil().getId());
        
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getDescricao()));
        }
        
        return authorities;
    }
}