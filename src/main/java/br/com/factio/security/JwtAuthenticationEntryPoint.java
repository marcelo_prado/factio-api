package br.com.factio.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import br.com.factio.enums.Mensagens;
import br.com.factio.payload.ApiResponse;
import br.com.factio.payload.ExceptionResponse;

/**
 * 
 * @author Grupo 2 - PDSI 
 * 
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationEntryPoint.class);

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpResponse,
        AuthenticationException e) throws IOException, ServletException {
        logger.error("Responding with unauthorized error. Message - {}", e.getMessage());

        ExceptionResponse errorBody = new ExceptionResponse("", e.getMessage());
        if (e.getCause() != null) {
            errorBody.setCause(e.getCause().getMessage());
        }

        ApiResponse<ExceptionResponse> response = new ApiResponse<>(Mensagens.Unauthorized, errorBody);
        httpResponse.setCharacterEncoding("ISO-8859-1");
        httpResponse.setContentType("application/json");
        httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        httpResponse.getOutputStream().println(new Gson().toJson(response));
    }
}
