package br.com.factio.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import br.com.factio.enums.Mensagens;
import br.com.factio.exception.BadRequestException;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.service.UsuarioService;
import br.com.factio.util.RestControllerUtil;

/**
 * Created by rajeevkumarsingh on 19/08/17.
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	@Autowired
	private JwtTokenProvider tokenProvider;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UsuarioDetailService usuarioDetailService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		String jwt = getJwtFromRequest(request);
		boolean tokenValido = false;
		if (StringUtils.hasText(jwt) && (tokenValido = tokenProvider.validateToken(jwt))) {
			Long userId = tokenProvider.getUserIdFromJWT(jwt);

			Usuario usuario = usuarioService.findById(userId);
			UserDetails userDetails = new UsuarioPrincipal(usuario, usuarioDetailService.getAuthorities(usuario));
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails,
					null, userDetails.getAuthorities());
			authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

			SecurityContextHolder.getContext().setAuthentication(authentication);
		} else if (StringUtils.hasText(jwt) && !tokenValido)
			throw new BadRequestException(Mensagens.TokenInvalido);

		filterChain.doFilter(request, response);
	}

	public static final String getJwtFromRequest(HttpServletRequest request) {
		final String[] AUTH_WHITELIST = { "/api/auth/login", "/api/auth/signin", "/api/usuarios/recuperarSenha",
				"/api/usuarios/alterarSenhaToken", "/api/usuarios/verificarEmail" };
		boolean pathPermitido = false;
		String path = RestControllerUtil.getPathFromContext();
		for (String s : AUTH_WHITELIST) {
			if (s.equals(path)) {
				pathPermitido = true;
				break;
			}
		}

		if (!pathPermitido) {
			String bearerToken = request.getHeader("Authorization");
			if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
				return bearerToken.substring(7, bearerToken.length());
			}
		}
		return null;
	}
}
