package br.com.factio;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import br.com.factio.config.ApplicationConfig;
import br.com.factio.service.impl.EmailServiceImpl;

public class EmailServiceTest {

	@InjectMocks
	private EmailServiceImpl emailServiceImpl;
	
	@Mock
	private JavaMailSender javaMailSender;
	
	@Mock
	private ApplicationConfig appConfig;
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
		Mockito.doNothing().when(javaMailSender).send(Mockito.any(SimpleMailMessage.class));
	}
	
	@Test
	public void sendEmail() {
		Mockito.when(appConfig.getSupportEmail()).thenReturn("teste");
		emailServiceImpl.sendEmail("teste", "teste", "teste");
	}
	@Test
	public void sendEmail_1() {
		emailServiceImpl.sendEmail(new SimpleMailMessage());
	}
}