package br.com.factio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import br.com.factio.exception.BadRequestException;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.SalvarAnuncioEventoRequest;
import br.com.factio.repository.AnuncioEventoRepository;
import br.com.factio.repository.MoradiaRepository;
import br.com.factio.repository.entity.AnuncioEvento;
import br.com.factio.repository.entity.Moradia;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.UsuarioService;
import br.com.factio.service.impl.AnuncioEventoServiceImpl;

public class AnuncioEventoServiceTest {

	@InjectMocks
	private AnuncioEventoServiceImpl anuncioEventoServiceImpl;
	
	@Mock
	private AnuncioEventoRepository anuncioEventoRepository;
	
	@Mock
	private MoradiaRepository moradiaRepository;
	
	@Mock
	private UsuarioService usuarioService;
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void salvarAnuncioEvento() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);
		
		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		
		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		SalvarAnuncioEventoRequest request = new SalvarAnuncioEventoRequest();
		request.setId(1L);
		request.setMoradiaId(1L);
		request.setCriadorId(1L);
		
		Optional<Moradia> optional = Optional.of(moradia);
		
		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);
		Mockito.when(usuarioService.findById(Mockito.anyLong())).thenReturn(new Usuario());
		
		anuncioEventoServiceImpl.salvarAnuncioEvento(userPrincipal, request);
	}
	
	@Test(expected = ValidationException.class)
	public void salvarAnuncioEvento_ValidationException_1() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);
		
		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		
		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		SalvarAnuncioEventoRequest request = new SalvarAnuncioEventoRequest();
		request.setId(1L);
		request.setMoradiaId(1L);
		request.setCriadorId(1L);
		
		Mockito.when(usuarioService.findById(Mockito.anyLong())).thenReturn(new Usuario());
		
		anuncioEventoServiceImpl.salvarAnuncioEvento(userPrincipal, request);
	}
	
	@Test(expected = ValidationException.class)
	public void salvarAnuncioEvento_ValidationException() {
		Moradia moradia = new Moradia();
		moradia.setId(2L);
		
		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		
		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		SalvarAnuncioEventoRequest request = new SalvarAnuncioEventoRequest();
		request.setId(1L);
		request.setMoradiaId(1L);
		request.setCriadorId(1L);
		
		Optional<Moradia> optional = Optional.of(moradia);
		
		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);
		anuncioEventoServiceImpl.salvarAnuncioEvento(userPrincipal, request);
	}
	
	@Test
	public void findById() {
		Moradia moradia = new Moradia();
		moradia.setId(2L);
		moradia.setBairro("teste");
		moradia.setApelido("apelido");
		moradia.setCep("cep");
		moradia.setCidade("cidade");
		moradia.setEndereco("end");
		moradia.setNumero(12);
		moradia.setComplemento("1");
		moradia.setEstado("estado");
		moradia.setDataCriacao(new Date());

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		
		AnuncioEvento anuncio = new AnuncioEvento();
		anuncio.setMoradia(moradia);
		anuncio.setCriador(user);
		
		Optional<AnuncioEvento> optional = Optional.of(anuncio);
		
		Mockito.when(anuncioEventoRepository.findById(Mockito.anyLong())).thenReturn(optional);
		Assert.assertNotNull(anuncioEventoServiceImpl.findById(1L));
	}
	
	@Test(expected = NullPointerException.class)
	public void findById_NullPointerException() {
		
		SalvarAnuncioEventoRequest request = new SalvarAnuncioEventoRequest();
		request.setId(1L);
		
		AnuncioEvento anuncio = new AnuncioEvento();
		Optional<AnuncioEvento> optional = Optional.of(anuncio);
		
		Mockito.when(anuncioEventoRepository.findById(Mockito.anyLong())).thenReturn(optional);
		Assert.assertNotNull(anuncioEventoServiceImpl.findById(1L));
	}
	
	@Test(expected = ValidationException.class)
	public void findById_ValidationException() {
		anuncioEventoServiceImpl.findById(1L);
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void findAll() {
		Moradia moradia = new Moradia();
		moradia.setId(2L);
		moradia.setBairro("teste");
		moradia.setApelido("apelido");
		moradia.setCep("cep");
		moradia.setCidade("cidade");
		moradia.setEndereco("end");
		moradia.setNumero(12);
		moradia.setComplemento("1");
		moradia.setEstado("estado");
		moradia.setDataCriacao(new Date());
		
		Map<String, String> filters = new HashMap<>();
		filters.put("cidade", "cidade");
		filters.put("estado", "estado");

		
		AnuncioEvento anuncio = new AnuncioEvento(1L,new Date(),"teste", "teste","teste","teste",new Date(),BigDecimal.ONE,moradia,new Usuario());
		List<AnuncioEvento>lstAnuncio = new ArrayList<>();
		lstAnuncio.add(anuncio);
		Mockito.when(anuncioEventoRepository.findAll()).thenReturn(lstAnuncio);
		
		
		
		Page<AnuncioEvento> page = new PageImpl(lstAnuncio);
		
		Mockito.when(anuncioEventoRepository.findByEstadoCidade(Mockito.anyString(),Mockito.anyString(),Mockito.any())).thenReturn(page);
		Assert.assertNotNull(anuncioEventoServiceImpl.findAll(filters,2,1));
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void findAll_1() {
		Moradia moradia = new Moradia();
		moradia.setId(2L);
		moradia.setBairro("teste");
		moradia.setApelido("apelido");
		moradia.setCep("cep");
		moradia.setCidade("cidade");
		moradia.setEndereco("end");
		moradia.setNumero(12);
		moradia.setComplemento("1");
		moradia.setEstado("estado");
		moradia.setDataCriacao(new Date());
		
		Map<String, String> filters = new HashMap<>();
		filters.put("estado", "estado");

		
		AnuncioEvento anuncio = new AnuncioEvento(1L,new Date(),"teste", "teste","teste","teste",new Date(),BigDecimal.ONE,moradia,new Usuario());
		List<AnuncioEvento>lstAnuncio = new ArrayList<>();
		lstAnuncio.add(anuncio);
		Mockito.when(anuncioEventoRepository.findAll()).thenReturn(lstAnuncio);
			
		Page<AnuncioEvento> page = new PageImpl(lstAnuncio);
		
		Mockito.when(anuncioEventoRepository.findByEstado(Mockito.anyString(),Mockito.any())).thenReturn(page);
		Assert.assertNotNull(anuncioEventoServiceImpl.findAll(filters,2,1));
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void findAll_2() {
		Moradia moradia = new Moradia();
		moradia.setId(2L);
		moradia.setBairro("teste");
		moradia.setApelido("apelido");
		moradia.setCep("cep");
		moradia.setCidade("cidade");
		moradia.setEndereco("end");
		moradia.setNumero(12);
		moradia.setComplemento("1");
		moradia.setEstado("estado");
		moradia.setDataCriacao(new Date());
		
		Map<String, String> filters = new HashMap<>();

		AnuncioEvento anuncio = new AnuncioEvento(1L,new Date(),"teste", "teste","teste","teste",new Date(),BigDecimal.ONE,moradia,new Usuario());
		List<AnuncioEvento>lstAnuncio = new ArrayList<>();
		lstAnuncio.add(anuncio);
		Mockito.when(anuncioEventoRepository.findAll()).thenReturn(lstAnuncio);
			
		Page<AnuncioEvento> page = new PageImpl(lstAnuncio);
		
		Mockito.when(anuncioEventoRepository.findAll(Mockito.any(Pageable.class))).thenReturn(page);
		Assert.assertNotNull(anuncioEventoServiceImpl.findAll(filters,2,1));
	}
	
	@Test(expected = BadRequestException.class)
	public void findAll_BadRequestException() {
		Map<String, String> filters = new HashMap<>();
		filters.put("cidade", "cidade");
		anuncioEventoServiceImpl.findAll(filters,2,1);
	}
	
	@Test
	public void findByNome() {
		Moradia moradia = new Moradia();
		moradia.setId(2L);
		moradia.setBairro("teste");
		moradia.setApelido("apelido");
		moradia.setCep("cep");
		moradia.setCidade("cidade");
		moradia.setEndereco("end");
		moradia.setNumero(12);
		moradia.setComplemento("1");
		moradia.setEstado("estado");
		AnuncioEvento anuncio = new AnuncioEvento(1L,new Date(),"teste", "teste","teste","teste",new Date(),BigDecimal.ONE,moradia,new Usuario());
	
		Optional<AnuncioEvento> optinonal = Optional.of(anuncio);
		
		Mockito.when(anuncioEventoRepository.findByNome(Mockito.anyString())).thenReturn(optinonal);
		
		Assert.assertNotNull(anuncioEventoServiceImpl.findByNome("Nome"));
	}
	
	@Test(expected = ValidationException.class)
	public void findByNome_ValidationException() {	
		anuncioEventoServiceImpl.findByNome("Nome");
	}
}
