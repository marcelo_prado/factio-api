package br.com.factio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.com.factio.exception.BadRequestException;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.SalvarTarefaRequest;
import br.com.factio.payload.VincularResponsaveisRequest;
import br.com.factio.repository.MoradiaRepository;
import br.com.factio.repository.TarefaRepository;
import br.com.factio.repository.UsuarioRepository;
import br.com.factio.repository.entity.Moradia;
import br.com.factio.repository.entity.ResponsavelTarefa;
import br.com.factio.repository.entity.Tarefa;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.UsuarioService;
import br.com.factio.service.impl.TarefaServiceImpl;

public class TarefaServiceTest {
	@InjectMocks
	private TarefaServiceImpl service;

	@Mock
	private UsuarioService usuarioService;

	@Mock
	private UsuarioRepository usuarioRepository;

	@Mock
	private TarefaRepository tarefaRepository;

	@Mock
	private MoradiaRepository moradiaRepository;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = ValidationException.class)
	public void salvarTarefa_ValidationException() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		user.setUltimoLogin(new Date());

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		SalvarTarefaRequest request = new SalvarTarefaRequest();
		request.setMoradiaId(2L);

		service.salvarTarefa(userPrincipal, request);
	}

	@Test(expected = ValidationException.class)
	public void salvarTarefa_ValidationException_1() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		user.setUltimoLogin(new Date());

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		SalvarTarefaRequest request = new SalvarTarefaRequest();
		request.setMoradiaId(1L);

		request.setDataConclusao(new Date());

		service.salvarTarefa(userPrincipal, request);
	}

	@Test
	public void salvarTarefa() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		user.setUltimoLogin(new Date());

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		SalvarTarefaRequest request = new SalvarTarefaRequest();
		request.setMoradiaId(1L);
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 5);
		dt = c.getTime();

		request.setDataConclusao(dt);
		List<Long> lstResponsaveis = new ArrayList<>();
		lstResponsaveis.add(1l);
		request.setResponsaveis(lstResponsaveis);

		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(moradia));

		Mockito.when(tarefaRepository.save(Mockito.any(Tarefa.class))).thenReturn(new Tarefa());

		Mockito.when(usuarioService.findById(Mockito.anyLong())).thenReturn(user);
		service.salvarTarefa(userPrincipal, request);
	}

	@Test
	public void findById() {
		Mockito.when(tarefaRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(new Tarefa(1L,"nome","desc","tipo",new Date(),new Moradia(),new ArrayList<>())));
		Assert.assertNotNull(service.findById(1L));
	}
	
	@Test
	public void findAll() {
		Tarefa tarefa = new Tarefa();
		tarefa.setId(1L);
		tarefa.setMoradia(new Moradia());
		
		List<Tarefa> lstTarefas = new ArrayList<>();
		lstTarefas.add(tarefa);
		Mockito.when(tarefaRepository.findAll()).thenReturn(lstTarefas);
		Assert.assertNotNull(service.findAll());
	}
	
	@Test(expected = ValidationException.class)
	public void vincularResponsaveis_ValidationException() {
		VincularResponsaveisRequest request = new VincularResponsaveisRequest();
		request.setDataConclusao(new Date());
		
		service.vincularResponsaveis(request);
	}
	@Test(expected = BadRequestException.class)
	public void vincularResponsaveis_BadRequestException() {
		VincularResponsaveisRequest request = new VincularResponsaveisRequest();
		
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 5);
		dt = c.getTime();
		request.setDataConclusao(dt);
		request.setTarefaId(1L);
		
		Tarefa tarefa = new Tarefa();
		List<ResponsavelTarefa> lstUser = new ArrayList<>();
		lstUser.add(new ResponsavelTarefa());
		tarefa.setUsuarios(lstUser);
		
		Mockito.when(tarefaRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(tarefa));
		service.vincularResponsaveis(request);
	}
	@Test
	public void vincularResponsaveis() {
		VincularResponsaveisRequest request = new VincularResponsaveisRequest();
		
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 5);
		dt = c.getTime();
		request.setDataConclusao(dt);
		request.setTarefaId(1L);
		
		List<Long> lstResponsaveis = new ArrayList<>();
		lstResponsaveis.add(1l);
		request.setResponsaveis(lstResponsaveis);
		
		Tarefa tarefa = new Tarefa();
		List<ResponsavelTarefa> lstUser = new ArrayList<>();
		tarefa.setUsuarios(lstUser);
		
		Mockito.when(tarefaRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(tarefa));
		
		Mockito.when(usuarioService.findById(Mockito.anyLong())).thenReturn(new Usuario());
		service.vincularResponsaveis(request);
	}
	
	@Test
	public void findAllByExample() {
		service.findAllByExample(null, null);
	}
}
