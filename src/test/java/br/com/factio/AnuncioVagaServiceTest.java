package br.com.factio;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import br.com.factio.exception.BadRequestException;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.SalvarAnuncioEventoRequest;
import br.com.factio.payload.SalvarAnuncioVagaRequest;
import br.com.factio.repository.AnuncioVagaRepository;
import br.com.factio.repository.MoradiaRepository;
import br.com.factio.repository.entity.AnuncioVaga;
import br.com.factio.repository.entity.Moradia;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.UsuarioService;
import br.com.factio.service.impl.AnuncioVagaServiceImpl;

public class AnuncioVagaServiceTest {

	@InjectMocks
	private AnuncioVagaServiceImpl anuncioVagaServiceImpl;
	
	@Mock
	private AnuncioVagaRepository anuncioVagaRepository;
	
	@Mock
	private MoradiaRepository moradiaRepository;
	
	@Mock
	private UsuarioService usuarioService;
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void salvarAnuncioVaga() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);
		
		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		
		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		SalvarAnuncioVagaRequest request = new SalvarAnuncioVagaRequest();
		request.setId(1L);
		request.setMoradiaId(1L);
		request.setCriadorId(1L);
		
		Optional<Moradia> optional = Optional.of(moradia);
		
		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);
		Mockito.when(usuarioService.findById(Mockito.anyLong())).thenReturn(new Usuario());
		
		anuncioVagaServiceImpl.salvarAnuncioVaga(userPrincipal, request);
	}
	
	@Test(expected = ValidationException.class)
	public void salvarAnuncioVaga_ValidationException() {
		Moradia moradia = new Moradia();
		moradia.setId(2L);
		
		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		
		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		SalvarAnuncioVagaRequest request = new SalvarAnuncioVagaRequest();
		request.setId(1L);
		request.setMoradiaId(1L);
		request.setCriadorId(1L);
		
		Optional<Moradia> optional = Optional.of(moradia);
		
		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);
		anuncioVagaServiceImpl.salvarAnuncioVaga(userPrincipal, request);
	}
	@Test(expected = ValidationException.class)
	public void salvarAnuncioVaga_ValidationException_1() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);
		
		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		
		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		SalvarAnuncioVagaRequest request = new SalvarAnuncioVagaRequest();
		request.setId(1L);
		request.setMoradiaId(1L);
		request.setCriadorId(1L);
		Mockito.when(usuarioService.findById(Mockito.anyLong())).thenReturn(new Usuario());
		
		anuncioVagaServiceImpl.salvarAnuncioVaga(userPrincipal, request);
	}
	
	@Test
	public void findById() {
		
		SalvarAnuncioEventoRequest request = new SalvarAnuncioEventoRequest();
		request.setId(1L);
		
		Moradia moradia = new Moradia();
		moradia.setId(2L);
		moradia.setBairro("teste");
		moradia.setApelido("apelido");
		moradia.setCep("cep");
		moradia.setCidade("cidade");
		moradia.setEndereco("end");
		moradia.setNumero(12);
		moradia.setComplemento("1");
		moradia.setEstado("estado");
		moradia.setDataCriacao(new Date());
		
		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		
		AnuncioVaga anuncio = new AnuncioVaga();
		anuncio.setCriador(user);
		anuncio.setDescricao("desc");
		anuncio.setId(1L);
		anuncio.setMoradia(moradia);
		anuncio.setObservacoes("obs");
		anuncio.setPublicoAlvo("publico");
		anuncio.setQtdQuartos(1);
		Optional<AnuncioVaga> optional = Optional.of(anuncio);
		
		Mockito.when(anuncioVagaRepository.findById(Mockito.anyLong())).thenReturn(optional);
		Assert.assertNotNull(anuncioVagaServiceImpl.findById(1L));
	}
	
	@Test(expected = ValidationException.class)
	public void findById_ValidationException() {
		Assert.assertNotNull(anuncioVagaServiceImpl.findById(1L));
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void findAll() {
		
		Moradia moradia = new Moradia();
		moradia.setId(2L);
		moradia.setBairro("teste");
		moradia.setApelido("apelido");
		moradia.setCep("cep");
		moradia.setCidade("cidade");
		moradia.setEndereco("end");
		moradia.setNumero(12);
		moradia.setComplemento("1");
		moradia.setEstado("estado");
		moradia.setDataCriacao(new Date());
		
		Map<String, String> filters = new HashMap<>();
		filters.put("cidade", "cidade");
		filters.put("estado", "estado");

		AnuncioVaga anuncio = new AnuncioVaga(1l,1,"teste","teste","teste",moradia,new Usuario());
		List<AnuncioVaga>lstAnuncio = new ArrayList<>();
		lstAnuncio.add(anuncio);
		Mockito.when(anuncioVagaRepository.findAll()).thenReturn(lstAnuncio);
		
		Page<AnuncioVaga> page = new PageImpl(lstAnuncio);
		
		Mockito.when(anuncioVagaRepository.findByEstadoCidade(Mockito.anyString(),Mockito.anyString(),Mockito.any())).thenReturn(page);
		Assert.assertNotNull(anuncioVagaServiceImpl.findAll(filters,2,1));
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void findAll_1() {
		
		Moradia moradia = new Moradia();
		moradia.setId(2L);
		moradia.setBairro("teste");
		moradia.setApelido("apelido");
		moradia.setCep("cep");
		moradia.setCidade("cidade");
		moradia.setEndereco("end");
		moradia.setNumero(12);
		moradia.setComplemento("1");
		moradia.setEstado("estado");
		moradia.setDataCriacao(new Date());
		
		Map<String, String> filters = new HashMap<>();
		filters.put("estado", "estado");

		AnuncioVaga anuncio = new AnuncioVaga(1l,1,"teste","teste","teste",moradia,new Usuario());
		List<AnuncioVaga>lstAnuncio = new ArrayList<>();
		lstAnuncio.add(anuncio);
		Mockito.when(anuncioVagaRepository.findAll()).thenReturn(lstAnuncio);
		
		Page<AnuncioVaga> page = new PageImpl(lstAnuncio);
		
		Mockito.when(anuncioVagaRepository.findByEstado(Mockito.anyString(),Mockito.any())).thenReturn(page);
		Assert.assertNotNull(anuncioVagaServiceImpl.findAll(filters,2,1));
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void findAll_2() {
		
		Moradia moradia = new Moradia();
		moradia.setId(2L);
		moradia.setBairro("teste");
		moradia.setApelido("apelido");
		moradia.setCep("cep");
		moradia.setCidade("cidade");
		moradia.setEndereco("end");
		moradia.setNumero(12);
		moradia.setComplemento("1");
		moradia.setEstado("estado");
		moradia.setDataCriacao(new Date());
		
		Map<String, String> filters = new HashMap<>();

		AnuncioVaga anuncio = new AnuncioVaga(1l,1,"teste","teste","teste",moradia,new Usuario());
		List<AnuncioVaga>lstAnuncio = new ArrayList<>();
		lstAnuncio.add(anuncio);
		Mockito.when(anuncioVagaRepository.findAll()).thenReturn(lstAnuncio);
		
		Page<AnuncioVaga> page = new PageImpl(lstAnuncio);
		
		Mockito.when(anuncioVagaRepository.findAll(Mockito.any(Pageable.class))).thenReturn(page);
		Assert.assertNotNull(anuncioVagaServiceImpl.findAll(filters,2,1));
	}
	@Test(expected = BadRequestException.class)
	public void findAll_BadRequestException() {
		
		Map<String, String> filters = new HashMap<>();
		filters.put("cidade", "cidade");
		anuncioVagaServiceImpl.findAll(filters,2,1);
	}
}
