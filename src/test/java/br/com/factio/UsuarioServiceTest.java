package br.com.factio;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import br.com.factio.exception.BadRequestException;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.AlterarSenhaRequest;
import br.com.factio.payload.AlterarSenhaTokenRequest;
import br.com.factio.payload.AtualizarUsuarioRequest;
import br.com.factio.payload.SalvarLocalizacaoRequest;
import br.com.factio.payload.SalvarUsuarioRequest;
import br.com.factio.repository.PerfilRepository;
import br.com.factio.repository.PerfilRoleRepository;
import br.com.factio.repository.UsuarioRepository;
import br.com.factio.repository.entity.Moradia;
import br.com.factio.repository.entity.Perfil;
import br.com.factio.repository.entity.PerfilRole;
import br.com.factio.repository.entity.ResetarSenhaToken;
import br.com.factio.repository.entity.Role;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.ResetarSenhaTokenService;
import br.com.factio.service.impl.UsuarioServiceImpl;

public class UsuarioServiceTest {

	@InjectMocks
	private UsuarioServiceImpl service;

	@Mock
	private UsuarioRepository usuarioRepository;

	@Mock
	private PerfilRoleRepository perfilRoleRepository;

	@Mock
	private PerfilRepository perfilRepository;

	@Mock
	private ResetarSenhaTokenService resetarSenhaTokenService;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void existsByEmail() {
		Optional<Usuario> optional = Optional.of(new Usuario());
		Mockito.when(usuarioRepository.findByEmail(Mockito.anyString())).thenReturn(optional);
		Assert.assertTrue(service.existsByEmail("teste"));
	}

	@Test
	public void atualizarUsuario() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		user.setUltimoLogin(new Date());

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		AtualizarUsuarioRequest request = new AtualizarUsuarioRequest();
		request.setId(1L);
		request.setAtivo(true);
		request.setNome("Nome");
		request.setPerfil(1L);
		request.setTelefone("tel");
		request.setUltimoLogin(new Date());

		Perfil perfil = new Perfil(1l, "desc");

		Optional<Perfil> optional = Optional.of(perfil);

		Optional<Usuario> optional1 = Optional.of(user);

		Mockito.when(perfilRepository.findById(Mockito.anyLong())).thenReturn(optional);
		Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(optional1);

		service.atualizarUsuario(userPrincipal, request);
	}

	@Test
	public void atualizarUsuario_1() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		AtualizarUsuarioRequest request = new AtualizarUsuarioRequest();
		request.setId(1L);
		request.setAtivo(true);
		request.setNome("Nome");
		request.setPerfil(1L);
		request.setTelefone("tel");

		Perfil perfil = new Perfil(1l, "desc");

		Optional<Perfil> optional = Optional.of(perfil);

		Optional<Usuario> optional1 = Optional.of(user);

		Mockito.when(perfilRepository.findById(Mockito.anyLong())).thenReturn(optional);
		Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(optional1);

		service.atualizarUsuario(userPrincipal, request);
	}

	@Test(expected = ValidationException.class)
	public void atualizarUsuario_ValidationException() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		user.setUltimoLogin(new Date());

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		AtualizarUsuarioRequest request = new AtualizarUsuarioRequest();
		request.setId(1L);
		request.setAtivo(true);
		request.setNome("Nome");
		request.setPerfil(1L);
		request.setTelefone("tel");
		request.setUltimoLogin(new Date());
		service.atualizarUsuario(userPrincipal, request);
	}

	@Test(expected = ValidationException.class)
	public void atualizarUsuario_ValidationException_1() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		user.setUltimoLogin(new Date());

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		AtualizarUsuarioRequest request = new AtualizarUsuarioRequest();
		request.setId(1L);
		request.setAtivo(true);
		request.setNome("Nome");
		request.setPerfil(1L);
		request.setTelefone("tel");
		request.setUltimoLogin(new Date());

		Optional<Usuario> optional1 = Optional.of(user);

		Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(optional1);

		service.atualizarUsuario(userPrincipal, request);
	}

	@Test(expected = BadRequestException.class)
	public void atualizarUsuario_BadRequestException() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		user.setUltimoLogin(new Date());

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		AtualizarUsuarioRequest request = new AtualizarUsuarioRequest();
		request.setId(2L);
		request.setAtivo(true);
		request.setNome("Nome");
		request.setPerfil(1L);
		request.setTelefone("tel");
		request.setUltimoLogin(new Date());
		service.atualizarUsuario(userPrincipal, request);
	}

	@Test(expected = ValidationException.class)
	public void salvarUsuario_ValidationException() {
		Usuario user = new Usuario();
		user.setId(1L);
		user.setUltimoLogin(new Date());
		Optional<Usuario> optional = Optional.of(user);
		Mockito.when(usuarioRepository.findByEmail(Mockito.any())).thenReturn(optional);
		service.salvarUsuario(new SalvarUsuarioRequest());
	}

	@Test(expected = ValidationException.class)
	public void salvarUsuario_ValidationException_1() {
		service.salvarUsuario(new SalvarUsuarioRequest());
	}
	@Test
	public void salvarUsuario() {
		Perfil perfil = new Perfil(1l, "desc");
		Optional<Perfil> optional1 = Optional.of(perfil);
		Mockito.when(perfilRepository.findById(Mockito.any())).thenReturn(optional1);
		service.salvarUsuario(new SalvarUsuarioRequest());
	}
	
	@Test
	public void alterarSenha() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		user.setUltimoLogin(new Date());
		user.setSenha("123");

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		AlterarSenhaRequest request = new AlterarSenhaRequest();
		request.setSenhaAntiga("123");
		request.setSenhaNova("1234");
		
		service.alterarSenha(userPrincipal, request);
	}
	
	@Test(expected = BadRequestException.class)
	public void alterarSenha_BadRequestException() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		user.setUltimoLogin(new Date());
		user.setSenha("1235");

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		AlterarSenhaRequest request = new AlterarSenhaRequest();
		request.setSenhaAntiga("123");
		request.setSenhaNova("1234");
		
		service.alterarSenha(userPrincipal, request);
	}
	
	@Test
	public void alterarSenhaToken() {
		ResetarSenhaToken reset = new ResetarSenhaToken();
		reset.setUsuario(new Usuario());
		
		Mockito.when(resetarSenhaTokenService.findByToken(Mockito.any())).thenReturn(new ResetarSenhaToken());
		Mockito.when(resetarSenhaTokenService.findByToken(Mockito.any())).thenReturn(reset);
		
		service.alterarSenhaToken(new AlterarSenhaTokenRequest());
	
	}
	
	@Test
	public void consultarPermissoesPorIdPerfil() {
		PerfilRole perfilRole = new PerfilRole();
		perfilRole.setRole(new Role());
		List<PerfilRole> lstPerfil = new ArrayList<>();
		lstPerfil.add(perfilRole);
		Mockito.when(perfilRoleRepository.findByPerfil(Mockito.any())).thenReturn(lstPerfil);
		service.consultarPermissoesPorIdPerfil(1L);
	}
	@Test
	public void consultarPermissoesPorIdPerfil_1() {
		service.consultarPermissoesPorIdPerfil(null);
	}
	
	@Test(expected = ValidationException.class)
	public void findByEmail_ValidationException() {
		service.findByEmail("emailTest");
	}
	
	@Test
	public void findByEmail() {
		Mockito.when(usuarioRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(new Usuario()));
		service.findByEmail("emailTest");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void findAllByExample() {
		Map<String, String> filters = new HashMap<>();
		filters.put("nome", "nome");
		filters.put("email", "email");
		filters.put("telefone", "tel");
		
		Perfil perfil = new Perfil(1L,"dsc");
		Usuario user = new Usuario();
		user.setId(1L);
		user.setNome("Nome");
		user.setTelefone("tel");
		user.setAtivo(true);
		user.setEmail("teste");
		user.setPerfil(new Perfil(1L,"dsc"));
		List<Usuario> lstUser = new ArrayList<>();
		lstUser.add(user);
		
		
		Page<Usuario> usuarios = new PageImpl<>(lstUser);
		Mockito.when(perfilRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(perfil));
		Mockito.when(usuarioRepository.findAll(Mockito.any(Example.class),Mockito.any(Pageable.class))).thenReturn(usuarios);
		
		service.findAllByExample(filters, 1, 1);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void findAllByExample_1() {
		Map<String, String> filters = new HashMap<>();
		filters.put("nome", "nome");
		filters.put("email", "email");
		filters.put("telefone", "tel");
		filters.put("perfil",	"1");
		filters.put("ativo",	"true");
		
		Perfil perfil = new Perfil(1L,"dsc");
		Usuario user = new Usuario();
		user.setId(1L);
		user.setNome("Nome");
		user.setTelefone("tel");
		user.setAtivo(true);
		user.setEmail("teste");
		user.setPerfil(new Perfil(1L,"dsc"));
		List<Usuario> lstUser = new ArrayList<>();
		lstUser.add(user);
		
		
		Page<Usuario> usuarios = new PageImpl<>(lstUser);
		Mockito.when(perfilRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(perfil));
		Mockito.when(usuarioRepository.findAll(Mockito.any(Example.class),Mockito.any(Pageable.class))).thenReturn(usuarios);
		
		service.findAllByExample(filters, 1, 1);
	}
	
	@Test(expected = BadRequestException.class)
	public void salvarLocalizacao_BadRequestException() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		user.setUltimoLogin(new Date());
		user.setSenha("1235");

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		SalvarLocalizacaoRequest request = new SalvarLocalizacaoRequest();
		request.setUsuarioId(2L);
		service.salvarLocalizacao(userPrincipal, request);
	}
	
	@Test
	public void salvarLocalizacao() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);
		user.setUltimoLogin(new Date());
		user.setSenha("1235");

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		SalvarLocalizacaoRequest request = new SalvarLocalizacaoRequest();
		request.setUsuarioId(1L);
		request.setHoraLocalizacao(new Date());
		request.setLatitude("lat");
		request.setLongitude("long");
		
		Optional<Usuario> optional1 = Optional.of(user);
		Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(optional1);
		service.salvarLocalizacao(userPrincipal, request);
	}
}
