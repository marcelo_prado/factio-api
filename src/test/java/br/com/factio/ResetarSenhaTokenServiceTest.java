package br.com.factio;

import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.com.factio.config.ApplicationConfig;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.RecuperarSenhaRequest;
import br.com.factio.repository.ResetarSenhaTokenRepository;
import br.com.factio.repository.entity.ResetarSenhaToken;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.service.EmailService;
import br.com.factio.service.UsuarioService;
import br.com.factio.service.impl.ResetarSenhaTokenServiceImpl;

public class ResetarSenhaTokenServiceTest {

	@InjectMocks
	private ResetarSenhaTokenServiceImpl service;
	
	@Mock
	private ResetarSenhaTokenRepository resetarSenhaTokenRepository;
	
	@Mock
	private ApplicationConfig appConfig;
	
	@Mock
	private UsuarioService usuarioService;
	
	@Mock
	private EmailService emailService;
	
	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void registraToken() {
		service.registraToken(new ResetarSenhaToken());
	}
	
	@Test
	public void resetarSenhaToken() {
		Mockito.when(usuarioService.findByEmail(Mockito.any())).thenReturn(new Usuario());
		Mockito.when(appConfig.getSupportEmail()).thenReturn("teste");
		Mockito.when(appConfig.getUrlfactioWeb()).thenReturn("teste");
		Mockito.when(appConfig.getUrlRecuperarSenha()).thenReturn("teste");
		
		service.resetarSenhaToken(new RecuperarSenhaRequest());
	}
	
	@Test
	public void validarToken() {
		ResetarSenhaToken resetSenhaToken = new ResetarSenhaToken();
		resetSenhaToken.setDataExpiracao(DateUtils.addHours(new Date(), 1));
		Optional<ResetarSenhaToken> optional = Optional.of(resetSenhaToken);
		Mockito.when(resetarSenhaTokenRepository.findByToken(Mockito.anyString())).thenReturn(optional);
		service.validarToken("token");
	}
	
	@Test(expected = ValidationException.class)
	public void validarToken_ValidationException() {
		ResetarSenhaToken resetSenhaToken = new ResetarSenhaToken();
		resetSenhaToken.setDataExpiracao(new Date());
		Optional<ResetarSenhaToken> optional = Optional.of(resetSenhaToken);
		Mockito.when(resetarSenhaTokenRepository.findByToken(Mockito.anyString())).thenReturn(optional);
		service.validarToken("token");
	}
	
	@Test(expected = ValidationException.class)
	public void validarToken_Nullpointer() {
		service.validarToken("token");
	}
	
	@Test
	public void deletarTokensExpirados() {
		service.deletarTokensExpirados(new Date());
	}
	@Test
	public void deletarToken() {
		service.deletarToken("token");
	}
	
	@Test(expected = ValidationException.class)
	public void findByUsuario_ValidationException() {
		service.findByUsuario(new Usuario());
	}
	
	@Test
	public void findByUsuario() {
		ResetarSenhaToken resetSenhaToken = new ResetarSenhaToken();
		resetSenhaToken.setDataExpiracao(new Date());
		Optional<ResetarSenhaToken> optional = Optional.of(resetSenhaToken);
		Mockito.when(resetarSenhaTokenRepository.findByUsuario(Mockito.any())).thenReturn(optional);
		service.findByUsuario(new Usuario());
	}
	
	@Test(expected = ValidationException.class)
	public void findByToken_ValidationException() {
		service.findByToken("token");
	}
	
	@Test
	public void findByToken() {
		ResetarSenhaToken resetSenhaToken = new ResetarSenhaToken();
		resetSenhaToken.setDataExpiracao(new Date());
		Optional<ResetarSenhaToken> optional = Optional.of(resetSenhaToken);
		Mockito.when(resetarSenhaTokenRepository.findByToken(Mockito.any())).thenReturn(optional);
		service.findByToken("token");
	}

}
