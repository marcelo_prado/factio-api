package br.com.factio;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.com.factio.exception.BadRequestException;
import br.com.factio.exception.ValidationException;
import br.com.factio.payload.AtualizarMoradiaRequest;
import br.com.factio.payload.InserirMembroRequest;
import br.com.factio.payload.SalvarMoradiaRequest;
import br.com.factio.repository.MoradiaRepository;
import br.com.factio.repository.SolicitacaoMoradiaRepository;
import br.com.factio.repository.UsuarioRepository;
import br.com.factio.repository.entity.Moradia;
import br.com.factio.repository.entity.Perfil;
import br.com.factio.repository.entity.SolicitacaoMoradia;
import br.com.factio.repository.entity.Usuario;
import br.com.factio.security.UsuarioPrincipal;
import br.com.factio.service.UsuarioService;
import br.com.factio.service.impl.MoradiaServiceImpl;

public class MoradiaServiceTest {

	@InjectMocks
	private MoradiaServiceImpl moradiaServiceImpl;

	@Mock
	private MoradiaRepository moradiaRepository;

	@Mock
	private UsuarioRepository usuarioRepository;

	@Mock
	private UsuarioService usuarioService;

	@Mock
	private SolicitacaoMoradiaRepository solicitacaoMoradiaRepository;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = BadRequestException.class)
	public void salvarMoradia_BadRequestException() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		SalvarMoradiaRequest salvarMoradiaRequest = new SalvarMoradiaRequest();
		salvarMoradiaRequest.setResponsavel(2L);

		moradiaServiceImpl.salvarMoradia(userPrincipal, salvarMoradiaRequest);
	}

	@Test(expected = BadRequestException.class)
	public void salvarMoradia_BadRequestException_1() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setMoradia(moradia);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		SalvarMoradiaRequest salvarMoradiaRequest = new SalvarMoradiaRequest();
		salvarMoradiaRequest.setResponsavel(1L);

		moradiaServiceImpl.salvarMoradia(userPrincipal, salvarMoradiaRequest);
	}

	@Test(expected = BadRequestException.class)
	public void salvarMoradia_BadRequestException_2() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		SalvarMoradiaRequest salvarMoradiaRequest = new SalvarMoradiaRequest();
		salvarMoradiaRequest.setResponsavel(1L);
		salvarMoradiaRequest.setApelido("Apelido");

		Optional<Moradia> optional = Optional.of(new Moradia());

		Mockito.when(moradiaRepository.findByApelido(Mockito.anyString())).thenReturn(optional);

		moradiaServiceImpl.salvarMoradia(userPrincipal, salvarMoradiaRequest);
	}

	@Test
	public void salvarMoradia() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		SalvarMoradiaRequest salvarMoradiaRequest = new SalvarMoradiaRequest();
		salvarMoradiaRequest.setResponsavel(1L);
		salvarMoradiaRequest.setApelido("Apelido");

		Mockito.when(usuarioService.findById(1L)).thenReturn(user);

		moradiaServiceImpl.salvarMoradia(userPrincipal, salvarMoradiaRequest);
	}

	@Test
	public void atualizarMoradia() {
		Usuario user = new Usuario();
		user.setId(1L);

		Moradia moradia = new Moradia();
		moradia.setId(1L);
		moradia.setResponsavel(user);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		AtualizarMoradiaRequest request = new AtualizarMoradiaRequest();
		request.setId(1L);
		Optional<Moradia> optional = Optional.of(moradia);

		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);

		moradiaServiceImpl.atualizarMoradia(userPrincipal, request);

	}

	@Test
	public void atualizarMoradia_1() {
		Usuario user = new Usuario();
		user.setId(1L);

		Moradia moradia = new Moradia();
		moradia.setId(1L);
		moradia.setResponsavel(user);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		AtualizarMoradiaRequest request = new AtualizarMoradiaRequest();
		request.setId(1L);
		request.setResponsavel(2L);
		Optional<Moradia> optional = Optional.of(moradia);

		Optional<Usuario> optional1 = Optional.of(user);

		Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(optional1);

		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);

		moradiaServiceImpl.atualizarMoradia(userPrincipal, request);

	}

	@Test(expected = BadRequestException.class)
	public void atualizarMoradia_BadRequestException() {
		Usuario user = new Usuario();
		user.setId(1L);

		Moradia moradia = new Moradia();
		moradia.setId(1L);
		moradia.setResponsavel(user);

		user.setMoradiaResponsavel(moradia);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		AtualizarMoradiaRequest request = new AtualizarMoradiaRequest();
		request.setId(1L);
		request.setResponsavel(2L);
		Optional<Moradia> optional = Optional.of(moradia);

		Optional<Usuario> optional1 = Optional.of(user);

		Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(optional1);

		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);

		moradiaServiceImpl.atualizarMoradia(userPrincipal, request);

	}

	@Test(expected = ValidationException.class)
	public void atualizarMoradia_ValidationException() {
		Usuario user = new Usuario();
		user.setId(1L);

		Moradia moradia = new Moradia();
		moradia.setId(1L);
		moradia.setResponsavel(user);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		AtualizarMoradiaRequest request = new AtualizarMoradiaRequest();
		request.setId(1L);
		request.setResponsavel(2L);
		Optional<Moradia> optional = Optional.of(moradia);

		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);

		moradiaServiceImpl.atualizarMoradia(userPrincipal, request);

	}

	@Test(expected = BadRequestException.class)
	public void atualizarMoradia_BadRequestException_1() {
		Usuario user = new Usuario();
		user.setId(1L);

		Usuario user1 = new Usuario();
		user.setId(2L);

		Moradia moradia = new Moradia();
		moradia.setId(1L);
		moradia.setResponsavel(user1);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		AtualizarMoradiaRequest request = new AtualizarMoradiaRequest();
		request.setId(1L);
		request.setResponsavel(2L);
		Optional<Moradia> optional = Optional.of(moradia);

		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);

		moradiaServiceImpl.atualizarMoradia(userPrincipal, request);

	}

	@Test(expected = ValidationException.class)
	public void findById_ValidationException() {
		moradiaServiceImpl.findById(1L);
	}

	@Test
	public void findAll() {
		Mockito.when(moradiaRepository.findAll()).thenReturn(new ArrayList<>());
		moradiaServiceImpl.findAll();
	}

	@Test
	public void inserirMembro() {
		Usuario user = new Usuario();
		user.setId(1L);

		Usuario user1 = new Usuario();
		user1.setId(1L);

		List<Usuario> lstUser = new ArrayList<>();
		lstUser.add(user);
		lstUser.add(user1);

		Moradia moradia = new Moradia();
		moradia.setId(1L);
		moradia.setResponsavel(user);
		moradia.setMembros(lstUser);
		user.setMoradiaResponsavel(moradia);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		InserirMembroRequest request = new InserirMembroRequest(1L, 1L);

		Mockito.when(usuarioService.findById(Mockito.anyLong())).thenReturn(user1);

		Optional<Moradia> optional = Optional.of(moradia);

		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);

		moradiaServiceImpl.inserirMembro(request, userPrincipal);

	}

	@Test(expected = BadRequestException.class)
	public void inserirMembro_BadRequestException() {
		Usuario user = new Usuario();
		user.setId(1L);

		Usuario user1 = new Usuario();
		user1.setId(2L);

		List<Usuario> lstUser = new ArrayList<>();
		lstUser.add(user);

		Moradia moradia = new Moradia();
		moradia.setId(1L);
		moradia.setResponsavel(user1);
		moradia.setMembros(lstUser);
		user.setMoradiaResponsavel(moradia);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		InserirMembroRequest request = new InserirMembroRequest(2L, 2L);

		Mockito.when(usuarioService.findById(Mockito.anyLong())).thenReturn(user);

		moradiaServiceImpl.inserirMembro(request, userPrincipal);
	}

	@Test(expected = BadRequestException.class)
	public void inserirMembro_BadRequestException_1() {
		Usuario user = new Usuario();
		user.setId(1L);

		List<Usuario> lstUser = new ArrayList<>();
		lstUser.add(user);

		Moradia moradia = new Moradia();
		moradia.setId(1L);
		moradia.setResponsavel(user);
		moradia.setMembros(lstUser);
		user.setMoradiaResponsavel(moradia);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		InserirMembroRequest request = new InserirMembroRequest(1L, 1L);

		Mockito.when(usuarioService.findById(Mockito.anyLong())).thenReturn(user);

		moradiaServiceImpl.inserirMembro(request, userPrincipal);
	}

	@Test(expected = BadRequestException.class)
	public void solicitarIngresso_BadRequestException() {
		Usuario user = new Usuario();
		user.setId(1L);
		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		moradiaServiceImpl.solicitarIngresso(new InserirMembroRequest(), userPrincipal);
	}

	@Test
	public void solicitarIngresso() {
		Usuario user = new Usuario();
		user.setId(1L);
		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		InserirMembroRequest request = new InserirMembroRequest(1L, 1L);

		Mockito.when(usuarioService.findById(Mockito.anyLong())).thenReturn(new Usuario());
		Optional<Moradia> optional = Optional.of(new Moradia());
		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);

		moradiaServiceImpl.solicitarIngresso(request, userPrincipal);
	}

	@Test(expected = BadRequestException.class)
	public void listarMembros_BadRequestException() {
		Usuario user = new Usuario();
		user.setId(1L);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		moradiaServiceImpl.listarMembros(1L, userPrincipal);
	}

	@Test
	public void listarMembros() {
		Usuario user = new Usuario();
		user.setId(1L);
		user.setNome("Nome");
		user.setTelefone("tel");
		user.setEmail("email");
		user.setAtivo(true);
		user.setUltimoLogin(new Date());
		user.setPerfil(new Perfil(1L, "desc"));

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);

		Moradia moradia = new Moradia();
		moradia.setId(1L);
		moradia.setResponsavel(user);
		user.setMoradia(moradia);
		user.setMoradiaResponsavel(moradia);

		List<Usuario> lstUser = new ArrayList<>();
		lstUser.add(user);
		moradia.setMembros(lstUser);

		Optional<Moradia> optional = Optional.of(moradia);

		Mockito.when(moradiaRepository.findById(Mockito.anyLong())).thenReturn(optional);
		moradiaServiceImpl.listarMembros(1L, userPrincipal);
	}

	@Test(expected = BadRequestException.class)
	public void listarSolicitacoes_BadRequestException() {
		Usuario user = new Usuario();
		user.setId(1L);
		user.setNome("Nome");
		user.setTelefone("tel");
		user.setEmail("email");
		user.setAtivo(true);
		user.setUltimoLogin(new Date());
		user.setPerfil(new Perfil(1L, "desc"));

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		moradiaServiceImpl.listarSolicitacoes(1L, userPrincipal);
	}

	@Test
	public void listarSolicitacoes() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setNome("Nome");
		user.setTelefone("tel");
		user.setEmail("email");
		user.setAtivo(true);
		user.setUltimoLogin(new Date());
		user.setPerfil(new Perfil(1L, "desc"));
		user.setMoradia(moradia);

		moradia.setResponsavel(user);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		ArrayList<SolicitacaoMoradia> lstSoliMoradia= new ArrayList<SolicitacaoMoradia>();
		lstSoliMoradia.add(new SolicitacaoMoradia(1L, moradia, user, new Date()));
		
		Mockito.when(solicitacaoMoradiaRepository.findByMoradiaId(Mockito.anyLong())).thenReturn(lstSoliMoradia);

		moradiaServiceImpl.listarSolicitacoes(1L, userPrincipal);
	}
	
	@Test(expected = BadRequestException.class)
	public void listarMoradiaPorMembro_BadRequestException() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);

		Usuario user = new Usuario();
		user.setId(1L);
		user.setNome("Nome");
		user.setTelefone("tel");
		user.setEmail("email");
		user.setAtivo(true);
		user.setUltimoLogin(new Date());
		user.setPerfil(new Perfil(1L, "desc"));
		user.setMoradia(moradia);

		moradia.setResponsavel(user);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		moradiaServiceImpl.listarMoradiaPorMembro(2L, userPrincipal);
	}
	
	@Test
	public void listarMoradiaPorMembro() {
		Moradia moradia = new Moradia();
		moradia.setId(1L);
		moradia.setBairro("bairro");
		moradia.setNumero(1);
		moradia.setComplemento("comp");
		moradia.setCidade("cidade");
		moradia.setEstado("estado");
		moradia.setCep("00000000");
		moradia.setDataCriacao(new Date());
		
		Usuario user = new Usuario();
		user.setId(1L);
		user.setNome("Nome");
		user.setTelefone("tel");
		user.setEmail("email");
		user.setAtivo(true);
		user.setUltimoLogin(new Date());
		user.setPerfil(new Perfil(1L, "desc"));
		user.setMoradia(moradia);

		moradia.setResponsavel(user);

		UsuarioPrincipal userPrincipal = new UsuarioPrincipal(user, null);
		
		Mockito.when(moradiaRepository.findByResponsavelId(Mockito.anyLong())).thenReturn(Optional.of(moradia));
		
		moradiaServiceImpl.listarMoradiaPorMembro(1L, userPrincipal);
	}
	
	@Test
	public void findAllByExample() {
		Map<String, String> filters = new HashMap<>();
		filters.put("nome", "nome");
		filters.put("endereco", "endereco");
		filters.put("bairro", "bairro");
		filters.put("cidade", "cidade");
		filters.put("estado", "estado");
		filters.put("cep", "cep");
		filters.put("responsavel", "1");
		
		Usuario user = new Usuario();
		user.setId(1L);
		user.setNome("Nome");
		user.setTelefone("tel");
		user.setEmail("email");
		user.setAtivo(true);
		user.setUltimoLogin(new Date());
		user.setPerfil(new Perfil(1L, "desc"));

		Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(user));
		
		Assert.assertNull(moradiaServiceImpl.findAllByExample(filters, null));
	}
	
	@Test
	public void findAllByExample_1() {
		Map<String, String> filters = new HashMap<>();
		filters.put("nome", "nome");
		filters.put("endereco", "endereco");
		filters.put("bairro", "bairro");
		filters.put("cidade", "cidade");
		filters.put("estado", "estado");
		filters.put("cep", "cep");
		filters.put("dataCriacao", "03/12/2018");
		filters.put("responsavel", "1");
		
		Usuario user = new Usuario();
		user.setId(1L);
		user.setNome("Nome");
		user.setTelefone("tel");
		user.setEmail("email");
		user.setAtivo(true);
		user.setUltimoLogin(new Date());
		user.setPerfil(new Perfil(1L, "desc"));

		Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(user));
		
		Assert.assertNull(moradiaServiceImpl.findAllByExample(filters, null));
	}
}
